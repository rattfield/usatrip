<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\TimeZone;
use App\Utilities\DiskUtility;
use App\Utilities\GpsUtility;
use Carbon\Carbon;

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('users', 'UserController');

Route::resource('days', 'DayController');

Route::resource('images', 'ImageController');

Route::get('test-image-data', function(){
    $image = Image::make('storage/PHOTO_20190312_150335.jpg')->exif();

    if(isset($image)) {
        if(isset($image["GPSLatitudeRef"]) && isset($image["GPSLongitudeRef"]))

            $gpsUtility = new GpsUtility();
            $timezone = $gpsUtility->setLatitude($image["GPSLatitudeRef"], $image['GPSLatitude'])
                ->setLongitude($image["GPSLongitudeRef"], $image['GPSLongitude'])
                ->setDateTime(new Carbon($image['DateTime']))
                ->getTimezone();

        return [
            "latitude" => $image['GPSLatitude'],
            'latitude_ref' => $image["GPSLatitudeRef"],
            "longitude" => $image['GPSLongitude'],
            'longitude_ref' => $image["GPSLongitudeRef"],
            "timezone" => $timezone
        ];
    } else {
        echo "No GPS Info";
    }
});
