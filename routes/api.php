<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {

    Route::get('users', ['as' => 'users.index', 'uses' => 'UserAPIController@index']);

    Route::get('users/{id}', ['as' => 'users.show', 'uses' => 'UserAPIController@show']);

    Route::get('days', ['as' => 'days.index', 'uses' => 'DayAPIController@index']);

    Route::get('days/{number}', ['as' => 'days.show', 'uses' => 'DayAPIController@show']);

    Route::get('images', ['as' => 'images.index', 'uses' => 'ImageAPIController@index']);

    Route::get('images/{id}', ['as' => 'images.show', 'uses' => 'ImageAPIController@show']);

});
