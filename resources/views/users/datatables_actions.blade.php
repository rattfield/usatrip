{!! Form::open(['route' => ['users.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('users.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    @if(!empty(Auth::user()) && (Auth::user()->is_admin || Auth::user()->id === $id))
    <a href="{{ route('users.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
        @if(\App\Models\User::adminUser()->id !== $id)
        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
           'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'onclick' => "return confirm('Are you sure?')"
        ]) !!}
        @endif
    @endif
</div>
{!! Form::close() !!}
