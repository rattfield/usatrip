<!-- File Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_name', 'File Name:') !!}
    {!! Form::text('file_name', null, ['class' => 'form-control']) !!}
</div>

<!-- File Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_path', 'File Path:') !!}
    {!! Form::text('file_path', null, ['class' => 'form-control']) !!}
</div>

<!-- File Date Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_date_time', 'File Date Time:') !!}
    {!! Form::number('file_date_time', null, ['class' => 'form-control']) !!}
</div>

<!-- File Size Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_size', 'File Size:') !!}
    {!! Form::number('file_size', null, ['class' => 'form-control']) !!}
</div>

<!-- File Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_width', 'File Width:') !!}
    {!! Form::number('file_width', null, ['class' => 'form-control']) !!}
</div>

<!-- File Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_height', 'File Height:') !!}
    {!! Form::number('file_height', null, ['class' => 'form-control']) !!}
</div>

<!-- Mime Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mime_type', 'Mime Type:') !!}
    {!! Form::text('mime_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('image_description', 'Image Description:') !!}
    {!! Form::textarea('image_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Orientation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orientation', 'Orientation:') !!}
    {!! Form::number('orientation', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_time', 'Date Time:') !!}
    {!! Form::date('date_time', null, ['class' => 'form-control','id'=>'date_time']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_time').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Date Time Original Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_time_original', 'Date Time Original:') !!}
    {!! Form::date('date_time_original', null, ['class' => 'form-control','id'=>'date_time_original']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_time_original').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Date Time Digitized Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_time_digitized', 'Date Time Digitized:') !!}
    {!! Form::date('date_time_digitized', null, ['class' => 'form-control','id'=>'date_time_digitized']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_time_digitized').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Exif Image Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('exif_image_width', 'Exif Image Width:') !!}
    {!! Form::number('exif_image_width', null, ['class' => 'form-control']) !!}
</div>

<!-- Exif Image Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('exif_image_height', 'Exif Image Height:') !!}
    {!! Form::number('exif_image_height', null, ['class' => 'form-control']) !!}
</div>

<!-- Gps Latitude Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gps_latitude_ref', 'Gps Latitude Ref:') !!}
    {!! Form::text('gps_latitude_ref', null, ['class' => 'form-control']) !!}
</div>

<!-- Gps Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gps_latitude', 'Gps Latitude:') !!}
    {!! Form::number('gps_latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Gps Longitude Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gps_longitude_ref', 'Gps Longitude Ref:') !!}
    {!! Form::text('gps_longitude_ref', null, ['class' => 'form-control']) !!}
</div>

<!-- Gps Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gps_longitude', 'Gps Longitude:') !!}
    {!! Form::number('gps_longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Gps Altitude Ref Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gps_altitude_ref', 'Gps Altitude Ref:') !!}
    {!! Form::text('gps_altitude_ref', null, ['class' => 'form-control']) !!}
</div>

<!-- Gps Altitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gps_altitude', 'Gps Altitude:') !!}
    {!! Form::number('gps_altitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Day Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('day_id', 'Day Id:') !!}
    {!! Form::number('day_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('images.index') !!}" class="btn btn-default">Cancel</a>
</div>
