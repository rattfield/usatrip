<div class="row" style="margin-left: 1em;">
    <div class="col-auto"
         style="margin-bottom: 1.25em;
         display: block;
         float:left;
         width: auto;">
    {!! \App\Utilities\ImageUtility::renderImage($image, 6) !!}
    </div>
    <div class="col-auto" style="width: auto; margin-left:1.25em; display: block; float:left;">

        <!-- Day Id Field -->
        <div class="form-inline">
            <div class="form-group">
            {!! Form::label('day_id', 'Day:', ['class' => 'col-form-label']) !!}
            {!! \App\Utilities\ImageUtility::renderImageDayLinkHtml($image) !!}.
            </div>
        </div>

        <!-- File Size Field -->
        <div class="form-inline">
            <div class="form-group">
            {!! Form::label('file_size', 'File Size:', ['class' => 'col-form-label']) !!}
            {!! \App\Utilities\DiskUtility::formatBytes($image->file_size, 3) !!}.
            </div>
        </div>

        <!-- File Width Field -->
        <div class="form-inline">
            <div class="form-group">
            {!! Form::label('file_width', 'Width:', ['class' => 'col-form-label']) !!}
            {!! number_format($image->file_width) !!} pixels.
            </div>
        </div>

        <!-- File Height Field -->
        <div class="form-inline">
            <div class="form-group">
            {!! Form::label('file_height', 'Height:') !!}
            {!! number_format($image->file_height) !!} pixels.
            </div>
        </div>

        @if(!empty($image->mime_type))
            <!-- Mime Type Field -->
            <div class="form-inline">
                <div class="form-group">
                {!! Form::label('mime_type', 'Mime Type:') !!}
                {!! $image->mime_type !!}.
                </div>
            </div>
        @endif

        @if(!empty($image->image_description))
            <!-- Image Description Field -->
            <div class="form-group">
                {!! Form::label('image_description', 'Image Description:') !!}
                <p>{!! $image->image_description !!}</p>
            </div>
        @endif

        @if(!empty($image->date_time))
            <!-- Date Time Field -->
            <div class="form-group">
                {!! Form::label('date_time', 'Date:') !!}
                <p>{!! \App\Utilities\ImageUtility::formatImageDate($image, true) !!}.</p>
            </div>

            @if(!empty($tzInfo))
            <div class="form-group">
                {!! Form::label('timezone', 'Timezone Information: ') !!}
                <ul style="margin-left: -1.5em;">
                    <li><em>Abbreviation</em>: {!! $tzInfo['abbreviation'] !!}.</li>
                    <li><em>Name</em>: {!! $tzInfo['timezone_name'] !!}.</li>
                    <li><em>Id</em>: {!! $tzInfo['timezone_id'] !!}.</li>
                </ul>
            </div>
            @endif
        @endif

        @if(!empty($image->gps_latitude))
            <!-- Gps Latitude Field -->
                <div class="form-group">
                    {!! Form::label('gps_latitude', 'Latitude:') !!}
                    <p>{!! $image->gps_latitude !!}
                        @if(!empty($image->gps_latitude_ref))
                            ({!! $image->gps_latitude_ref !!})
                        @endif.
                    </p>
                </div>
        @endif

        @if(!empty($image->gps_longitude))
            <!-- Gps Longitude Field -->
            <div class="form-group">
                {!! Form::label('gps_longitude', 'Longitude:') !!}
                <p>{!! $image->gps_longitude !!}
                    @if(!empty($image->gps_longitude_ref))
                        ({!! $image->gps_longitude_ref !!})
                    @endif.
                </p>
            </div>
        @endif

    @if(!empty($image->gps_altitude))
        <!-- Gps Altitude Field -->
        <div class="form-inline">
            <div class="form-group">
            {!! Form::label('gps_altitude', 'Elevation*:', ['class' => 'col-form-label']) !!}
            {!! number_format($image->gps_altitude) !!} meters.
            <p><small>* above / below sea level.</small></p>
            </div>
        </div>
    </div>
    @endif

    <div class="col-auto"
        style="float:left;
        width: auto;
        border: 1px solid black;
        padding: 1em;
        display: block;">
        Google Map will go here.
    </div>
</div>


