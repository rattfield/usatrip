<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        Schema::disableForeignKeyConstraints();

        $this->call(UsersTableSeeder::class);
        $this->call(TimezoneSeeder::class);
        $this->call(DaysTableSeeder::class);

        Schema::enableForeignKeyConstraints();
        Eloquent::reguard();
    }
}
