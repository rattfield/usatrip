<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        try{
            User::truncate();
            DB::beginTransaction();
            $user = User::create(
                [
                'name' => Config::get('app.admin_firstname') . ' ' . Config::get('app.admin_lastname'),
                'email' => Config::get('app.admin_email'),
                'password' => Config::get('app.admin_password'),
                'is_admin' => true,
                'email_verified_at' => Carbon::now(),
                'remember_token' => Str::random(60)
                ]
            );
            $user->save();
            DB::commit();

            $this->command->info("Successfully seeded users!");
        } catch (Exception $e){
            DB::rollBack();
            $this->command->error("Oops! An error occurred. Error info: " . $e->getMessage());
        }
    }
}
