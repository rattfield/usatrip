<?php

use App\Models\Day;
use App\Models\Image;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class DaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            Day::truncate();
            Image::truncate();
            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
            $this->command->error("The 'days' table could not be truncated. Error info: " . $e->getMessage());
        }

        $mainStartTime = Carbon::now();

        $date = Carbon::create(2019, 3, 10);

        for($i = 0; $i <= 10; $i++){

            $day = factory(Day::class)->make()->toArray();
            $day['number'] = $i;
            $day['title'] = "Day " . $i . '.';
            $day['date'] = $date->addDays($i)->format('Y-m-d');

            try {
                $dayStartTime = Carbon::now();

                DB::beginTransaction();
                $newDay = Day::create($day);
                $newDay->save();
                DB::commit();

                $this->command->info("Successfully seeded Day " . $newDay->number . ".");

                Artisan::call("images:process", ["dayNumber" => $newDay->number]);

                $countImages = count($newDay->images()->get());

                $this->command->info("Successfully processed {$countImages} images for Day " . $newDay->number);

                $dayFinishTime = Carbon::now();

                $durationInSeconds = $dayFinishTime->diffInSeconds($dayStartTime);
                $formatted = CarbonInterval::seconds($durationInSeconds)->cascade()->forHumans();

                $this->command->info("Time taken to process Day " . $newDay->number . " images: {$formatted}.\n");


            } catch (Exception $e){
                DB::rollBack();
                $this->command->error("Oops! An error occurred. Error info: " . $e->getMessage());
            }
        }

        $mainFinishTime = Carbon::now();

        $this->command->info("\nStart time: " . $mainStartTime->format('Y-m-d H:i:s') . '.');
        $this->command->info("Finish time: " . $mainFinishTime->format('Y-m-d H:i:s') . '.');

        $durationInSeconds = $mainFinishTime->diffInSeconds($mainStartTime);
        $formatted = CarbonInterval::seconds($durationInSeconds)->cascade()->forHumans();

        $this->command->info("Total duration: {$formatted}.\n");
    }
}
