<?php

use App\Models\TimeZone;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimezoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        try{
            TimeZone::truncate();
            DB::beginTransaction();

            $timezones = [
                [
                    "dst_offset" => 0,
                    "raw_offset" => -28800,
                    "timezone_id" => "America/Los_Angeles",
                    "timezone_name" => "Pacific Standard Time",
                    "abbreviation" => "PST"
                ],
                [
                    "dst_offset" => 3600,
                    "raw_offset" => -25200,
                    "timezone_id" => "America/Los_Angeles",
                    "timezone_name" => "Pacific Daylight Time",
                    "abbreviation" => "PDT"
                ],
                [
                    "dst_offset" => 0,
                    "raw_offset" => -25200,
                    "timezone_id" => "America/Phoenix",
                    "timezone_name" => "Mountain Standard Time",
                    "abbreviation" => "MST"
                ],
                [
                    "dst_offset" => 3600,
                    "raw_offset" => -21600,
                    "timezone_id" => "America/Phoenix",
                    "timezone_name" => "Mountain Daylight Time",
                    "abbreviation" => "MDT"
                ],
                [
                    "dst_offset" => 0,
                    "raw_offset" => -25200,
                    "timezone_id" => "America/Denver",
                    "timezone_name" => "Mountain Standard Time",
                    "abbreviation" => "MST"
                ],
                [
                    "dst_offset" => 3600,
                    "raw_offset" => -21600,
                    "timezone_id" => "America/Denver",
                    "timezone_name" => "Mountain Daylight Time",
                    "abbreviation" => "MDT"
                ],
                [
                    "dst_offset" => 0,
                    "raw_offset" => 43200,
                    "timezone_id" => "Pacific/Auckland",
                    "timezone_name" => "New Zealand Standard Time",
                    "abbreviation" => "NZST"
                ],
                [
                    "dst_offset" => 3600,
                    "raw_offset" => 46800,
                    "timezone_id" => "Pacific/Auckland",
                    "timezone_name" => "New Zealand Daylight Time",
                    "abbreviation" => "NZDT"
                ]
            ];

            $timezones = TimeZone::cleanMultipleInput($timezones);

            for ($i = 0; $i < count($timezones); $i++){
                $t = TimeZone::create($timezones[$i]);
                $t->save();
            }

            DB::commit();
            $this->command->info("Successfully seeded timezones!");

        } catch (Exception $e){
            DB::rollBack();
            $this->command->error("Oops! An error occurred. Error info: " . $e->getMessage());
        }
    }
}
