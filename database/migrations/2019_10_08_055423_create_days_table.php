<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'days', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->mediumInteger('number')->unique()->index('number');
                $table->string('title', 255)->unique()->index('title');
                $table->date('date')->unique()->index('date');
                $table->string('summary', 255)->nullable();
                $table->string('keywords', 255)->nullable();
                $table->text('description')->nullable();
                $table->string('slug');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('days');
    }
}
