<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimezonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'timezones', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->smallInteger("dst_offset")->unsigned();
                $table->mediumInteger("raw_offset");
                $table->string("timezone_id");
                $table->string("timezone_name");
                $table->string("abbreviation", 6);
                $table->unique(
                    ["id",
                    "timezone_id",
                    "timezone_name",
                    "abbreviation"
                    ], "unique_timezone_index"
                );
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timezones');
    }
}
