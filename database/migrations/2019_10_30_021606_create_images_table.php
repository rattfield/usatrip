<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'images', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('file_name')->unique();
                $table->string('file_path')->unique();
                $table->integer('file_size')->unsigned();
                $table->integer('file_width')->unsigned();
                $table->integer('file_height')->unsigned();
                $table->integer('file_date_time')->unsigned(); // Unix time format.
                $table->string('thumbnail_file_name')->unique();
                $table->string('thumbnail_file_path')->unique();
                $table->integer('thumbnail_file_width')->unsigned();
                $table->integer('thumbnail_file_height')->unsigned();
                $table->string('mime_type')->nullable();
                $table->text('image_description')->nullable();
                $table->smallInteger('orientation')->nullable()->unsigned();
                $table->dateTime('date_time');
                $table->string('gps_latitude_ref')->nullable();
                $table->float('gps_latitude', 18, 16)->nullable();
                $table->string('gps_longitude_ref')->nullable();
                $table->float('gps_longitude', 19, 16)->nullable();
                $table->string('gps_altitude_ref')->nullable();
                $table->integer('gps_altitude')->nullable();

                $table->unsignedBigInteger('day_id');

                $table->unsignedBigInteger('timezone_id')->nullable();

                $table->foreign('day_id')
                    ->references('id')
                    ->on('days');

                $table->foreign('timezone_id')
                    ->references('id')
                    ->on('timezones');

                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'images', function (Blueprint $table) {
                $table->dropForeign(['day_id']);
            }
        );

        Schema::dropIfExists('images');
    }
}
