<?php

/**
 * @var Factory $factory 
 */

use App\Models\Day;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(
    Day::class, function (Faker $faker) {
        $number = 0;
        $date = Carbon::create(2019, 3, 10);
        return [
        'number' => $number,
        'title' => "Day " . $number . '.',
        'date' => $date->format('Y-m-d'),
        'summary' => $faker->text(255),
        'keywords' => implode(',', $faker->words(10)),
        'description' => $faker->text(500)
        ];
    }
);
