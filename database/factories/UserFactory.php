<?php

/**
 * @var Factory $factory
 */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(
    User::class, function (Faker $faker) {

        return [
        'name' => $faker->firstName . ' ' . $faker->lastName,
        'email' => $faker->safeEmail,
        'email_verified_at' => $faker->date('Y-m-d H:i:s'),
        'password' => 'P@s5w0rD259',
        'is_admin' => 0,
        'remember_token' => Str::random(40)
        ];
    }
);
