<?php namespace Tests\Repositories;

use App;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\APIs\ApiTestTrait;
use Tests\TestCase;
use Laracasts\Flash\Flash;

class UserRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->userRepo = App::make(UserRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_user()
    {
        $user = factory(User::class)->make()->toArray();
        $user['password'] = bcrypt('P@s5w0rD259');

        $user = $this->userRepo->create($user);

        $createdUser = $user->toArray();

        $this->assertArrayHasKey('id', $createdUser);
        $this->assertNotNull($createdUser['id'], 'Created User must have id specified');
        $this->assertNotNull(User::find($createdUser['id']), 'User with given id must be in DB');
        $this->assertModelData($user->toArray(), $createdUser);

        !empty($user) ? $user->forceDelete() : null;
    }

    /**
     * @test read
     */
    public function test_read_user()
    {
        $user = User::first();

        $dbUser = $this->userRepo->find($user->id);

        $dbUser = $dbUser->toArray();
        $this->assertModelData($user->toArray(), $dbUser);
    }

    /**
     * @test update
     */
    public function test_update_user()
    {
        $user = factory(User::class)->create();
        $user->email = "Bob.Bobface@example.com";
        $fakeUser = $user->toArray();

        $updatedUser = $this->userRepo->update($fakeUser, $user->id);

        //$this->assertModelData($fakeUser, $updatedUser->toArray());
        $dbUser = $this->userRepo->find($user->id);
        $this->assertModelData($fakeUser, $dbUser->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_user()
    {
        $user = factory(User::class)->create();

        $resp = $this->userRepo->delete($user->id);

        $this->assertTrue($resp);
        $this->assertNull(User::find($user->id), 'User should not exist in DB');
    }

    public function test_get_fields_searchable()
    {

        $expected = [
            'name',
            'email',
            'is_admin'
        ];

        $actual = $this->userRepo->getFieldsSearchable();

        $this->assertNotNull($actual, "Fields searchable must not be null.");
        $this->assertEqualsCanonicalizing($expected, $actual);
    }

    private function assertModelData(Array $actualData, Array $expectedData)
    {
        foreach ($actualData as $key => $value) {
            if (in_array($key, ['created_at', 'updated_at'])) {
                continue;
            }
            if($key !== "password"){
                $this->assertEquals($actualData[$key], $expectedData[$key]);
            }
        }
    }
}
