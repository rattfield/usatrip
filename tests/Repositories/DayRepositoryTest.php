<?php namespace Tests\Repositories;

use App;
use App\Models\Day;
use App\Repositories\DayRepository;
use Illuminate\Support\Facades\DB;
use Tests\APIs\ApiTestTrait;
use Tests\TestCase;
use Throwable;
Use Exception;

class DayRepositoryTest extends TestCase
{
    use ApiTestTrait;

    /**
     * @var DayRepository
     */
    protected $dayRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->dayRepo = App::make(DayRepository::class);
    }

    public function test_create_day()
    {
        try {
            $day = $this->generateNewDayData();
            try {
                DB::beginTransaction();
                $createdDay = $this->dayRepo->create($day);
                DB::commit();
            } catch(Exception $e){
                DB::rollBack();
                echo("A Day instance could not be created. Error info: " . $e->getMessage());
            }

            $createdDayArray = $createdDay->toArray();
            $this->assertArrayHasKey('id', $createdDayArray);
            $this->assertNotNull($createdDay['id'], 'Created Day must have id specified');
            $this->assertNotNull(Day::find($createdDay['id']), 'Day with given id must be in DB');
            $this->assertModelData($day, $createdDayArray);

            try {
                DB::beginTransaction();
                $createdDay->forceDelete();
                DB::commit();
            } catch(Exception $e){
                DB::rollBack();
                echo("The created Day instance could not be deleted. Error info: " . $e->getMessage());
            }

        } catch(Exception $e){
            echo("Oops! An error occured. Error info: " . $e->getMessage());
        }
    }

    public function test_read_day()
    {
        try {
            $day = Day::first();
            $dbDay = $this->dayRepo->find($day->number);
            $dbDay = $dbDay->toArray();
            $this->assertModelData($day->toArray(), $dbDay);
        } catch(Exception $e){
            echo("Oops! An error occured. Error info: " . $e->getMessage());
        }
    }

    public function test_update_day()
    {
        try {
            $day = $this->generateNewDayData();

            try {
                DB::beginTransaction();
                $day = $this->dayRepo->create($day);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $day->forceDelete();
                echo("A Day instance could not be created. Error info: " . $e->getMessage());
            }

            try {
                $fakeDay = $this->generateNewDayData();

                DB::beginTransaction();
                $updatedDay = $this->dayRepo->update($fakeDay, $day->number);
                DB::commit();

                $this->assertModelData($fakeDay, $updatedDay->toArray());
                $dbDay = $this->dayRepo->findByField('number', $fakeDay['number'])->first();

                $this->assertModelData($fakeDay, $dbDay->toArray());
            } catch (Exception $e) {
                DB::rollBack();
                echo("An error occurred while updating a Day instance. Error info: " . $e->getMessage());
            }

            try {
                DB::beginTransaction();
                $day->forceDelete();
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                echo("The created Day instance could not be force-deleted. Error info: " . $e->getMessage());
            }
        } catch(Exception $e){
            echo("Oops! An error occured. Error info: " . $e->getMessage());
        }
    }

    public function test_delete_day()
    {
        try {
            $day = $this->generateNewDayData();

            try {
                DB::beginTransaction();
                $day = $this->dayRepo->create($day);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                echo("A Day instance could not be created. Error info: " . $e->getMessage());
            }

            try {
                DB::beginTransaction();
                $resp = $this->dayRepo->delete($day->number);
                DB::commit();
                $this->assertTrue($resp);
                $this->assertNull(Day::where('number', '=', $day->number)->first(), 'Day should not exist in DB');
            } catch (Exception $e) {
                DB::rollBack();
                echo("An error occurred with deleting a Day instance. Error info: " . $e->getMessage());
            }

            try {
                DB::beginTransaction();
                Day::where('deleted_at', '!=', null)
                    ->where('number', '=', $day->number)
                    ->forceDelete();
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                echo("The created Day instance could not be force-deleted. Error info: " . $e->getMessage());
            }
        } catch(Exception $e){
            echo("Oops! An error occured. Error info: " . $e->getMessage());
        }

    }

    public function test_get_fields_searchable()
    {

        $expected = [
            'number',
            'title',
            'date',
            'summary',
            'keywords',
            'description'
        ];

        $actual = $this->dayRepo->getFieldsSearchable();

        $this->assertNotNull($actual, "Fields searchable must not be null.");
        $this->assertEqualsCanonicalizing($expected, $actual);
    }

    public function tearDown(): void
    {
        try {
            parent::tearDown();
        } catch (Throwable $e) {
            echo("ERROR! Exception caused by: " . $e->getMessage());
        }
    }
}
