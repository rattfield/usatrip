<?php

namespace Test\App\Http\Controllers;

use App\Http\Controllers\UserController;
use App\Models\User;
use Exception;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ViewErrorBag;
use Tests\TestCase;

/**
 * Class UserControllerTest.
 *
 * @covers \App\Http\Controllers\UserController
 */
class UserControllerTest extends TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function test_index_view_as_guest()
    {
        $response = $this->call('GET', route("users.index"));

        $this->assertEquals(200, $response->status());
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
        $response->assertSee($this->usersDataTableIndexHeaderRow());
    }

    public function test_index_view_as_admin_user()
    {
        $adminUser = User::adminUser();

        $response = $this->actingAs($adminUser)
            ->call('GET', route("users.index"));

        $this->assertEquals(200, $response->status());
        $response->assertSeeText('Users');
        $response->assertSeeText('Add New');
        $response->assertSeeText($adminUser->name);
        $response->assertSeeText('Profile');
        $response->assertSeeText('Sign out');
        $response->assertDontSeeText('Login');
        $response->assertDontSeeText('Register');
        $response->assertSee($this->usersDataTableIndexHeaderRow());
    }

    public function test_show_user_view_as_guest_user()
    {
        $adminUser = User::adminUser();

        $this->generateShowUserTest($adminUser);
    }

    public function test_show_user_view_as_admin_user()
    {
        $userToView = User::adminUser();
        $viewingAsUser = User::adminUser();
        $this->generateShowUserTest($userToView, $viewingAsUser);
    }

    public function test_show_user_view_as_non_admin_user()
    {
        $userToView = User::adminUser();
        $viewingAsUser = factory(User::class)->create();
        $this->generateShowUserTest($userToView, $viewingAsUser);
    }

    public function test_show_non_existent_user_view()
    {
        $response = $this->call('GET', route("users.show", 123456789));
        $response->assertStatus(404);
    }

    public function test_destroy_user_as_guest_user()
    {
        $this->generateDeleteUserTest();
    }

    public function test_destroy_user_as_admin_user()
    {
        $this->generateDeleteUserTest(User::adminUser());
    }

    public function test_destroy_user_as_non_admin_user()
    {
        //Test a non-admin user deleting another non-admin user
        $userToDelete = $this->generateUser();
        $userWhoIsDeleting = $this->generateUser();
        $response = $this->actingAs($userWhoIsDeleting)
            ->call(
                'POST',
                route("users.destroy", $userToDelete->id),
                ['_token' => csrf_token(), '_method' => 'DELETE']
            );
        $response->assertRedirect(route("users.index"));

        $userWhoIsDeleting->forceDelete();
        $userToDelete->forceDelete();
    }

    public function test_show_create_user_view_as_guest_user()
    {
        $response = $this->call('GET', route("users.create"))
            ->assertRedirect(route("login"));
        $this->assertEquals(302, $response->status());
    }

    public function test_show_create_user_view_as_admin_user()
    {
        $response = $this->actingAs(User::adminUser())
            ->call('GET', route("users.create"));
        $this->assertEquals(200, $response->status());

        $response->assertSee('<form method="POST" action="' . route("users.store") . '" accept-charset="UTF-8">');
        $response->assertSee('<input name="_token" type="hidden" value="' . csrf_token() . '">');

        $response->assertSee('<label for="name">Name:</label>');
        $response->assertSee('<input class="form-control" name="name" type="text" id="name">');

        $response->assertSee('<label for="email">Email:</label>');
        $response->assertSee('<input class="form-control" name="email" type="email" id="email">');

        $response->assertSee('<label for="password">Password:</label>');
        $response->assertSee('<input class="form-control" name="password" type="password" value="" id="password">');

        $response->assertSee('<input class="btn btn-primary" type="submit" value="Save">');
    }

    public function test_store_user_as_guest_user()
    {
        $userData = $this->generateUserData();
        $response = $this->call('POST', route("users.store", $userData));
        $response->assertRedirect(route("login"));
    }

    public function test_store_user_as_standard_user()
    {
        $standardUser = $this->generateUser();
        $userData = $this->generateUserData();
        $response = $this->actingAs($standardUser)
            ->call(
                'POST',
                route("users.store", $userData),
                ['_token' => csrf_token()]
            );
        $response->assertRedirect(route("users.index"));
        $this->assertEquals(302, $response->status());
    }

    public function test_store_user_as_admin_user()
    {
        $userData = $this->generateUserData();
        $response = $this->actingAs(User::adminUser())
            ->call(
                'POST',
                route("users.store", $userData),
                ['_token' => csrf_token()]
            );
        $response->assertRedirect(route("users.index"));
        $this->assertEquals(302, $response->status());
    }

    public function test_show_edit_user_view_as_guest_user()
    {
        $user = $this->generateUser();
        $response = $this->call('GET', route("users.edit", $user->id));
        $response->assertRedirect(route("login"));
        $user->forceDelete();
    }

    public function test_show_edit_user_view_as_standard_user()
    {
        $user = $this->generateUser();
        $this->generateUserEditTest($user, $user);

        $otherUser = $this->generateUser();
        $this->generateUserEditTest($user, $otherUser);

        $user->forceDelete();
        $otherUser->forceDelete();
    }

    public function test_show_edit_user_view_as_admin_user()
    {
        $adminUser = User::adminUser();
        $user = $this->generateUser();
        $this->generateUserEditTest($user, $adminUser);
        $this->generateUserEditTest($adminUser, $adminUser);

        $user->forceDelete();
    }

    public function test_show_edit_non_existent_user_view()
    {
        $adminUser = User::adminUser();
        $response = $this->actingAs($adminUser)
            ->call('GET', route("users.edit", 123456789));
        $response->assertRedirect(route("users.index"));
        $this->assertEquals(302, $response->status());
    }

    public function test_update_user()
    {
        $userToUpdate = $this->generateUser();
        $this->generateUserUpdateTest($userToUpdate, $userToUpdate);

        $userToUpdate = $this->generateUser();
        $this->generateUserUpdateTest($userToUpdate, User::adminUser());

        $otherAuthUser = $this->generateUser();
        $this->generateUserUpdateTest($userToUpdate, $otherAuthUser);

        // Non-existent user.
        $response = $this->actingAs(User::adminUser())
            ->patch(
                route("users.update", 123456789),
                $this->generateUserData()
        );
        $response->assertRedirect(route("users.index"));
        $this->assertEquals(302, $response->status());
        $response->assertSessionHasNoErrors();
    }

    private function generateUserUpdateTest(User $userToUpdate, User $authUser = null)
    {
        $actingAs = !empty($authUser) ? $this->actingAs($authUser) : $this;

        $userData = $this->generateUserData();
        $userData['_token'] = csrf_token();

        $response = $actingAs->patch(route("users.update", $userToUpdate->id),
                $userData
            );

        if(empty($authUser)){
            $this->assertEquals(302, $response->status());
            $response->assertRedirect(route("login"));
        } else if(!$authUser->is_admin && $userToUpdate->id !== $authUser->id){
            $this->assertEquals(403, $response->status());
        } else if($authUser->is_admin && $userToUpdate->id !== $authUser->id){
            $response->assertRedirect(route("users.index"));
            $this->assertEquals(302, $response->status());
        }
    }

    private function generateUserEditTest(User $userToEdit, User $userViewingPage)
    {
        $response = $this->actingAs($userViewingPage)
            ->call('GET', route("users.edit", $userToEdit->id));

        if($userViewingPage->id !== $userToEdit->id && !$userViewingPage->is_admin){
            $response->assertRedirect(route("users.index"));
            $this->assertEquals(302, $response->status());
        }

        if($userViewingPage->id === $userToEdit->id || $userViewingPage->is_admin)
        {
            $this->assertEquals(200, $response->status());

            $response->assertSee('<form method="POST" action="' . route("users.update", $userToEdit->id) .'" accept-charset="UTF-8">');
            $response->assertSee('<input name="_token" type="hidden" value="' . csrf_token() . '">');

            $response->assertSee('<label for="name">Name:</label>');
            $response->assertSee('<input class="form-control" name="name" type="text" value="' . $userToEdit->name .'" id="name">');

            $response->assertSee('<label for="email">Email:</label>');
            $response->assertSee('<input class="form-control" name="email" type="email" value="' . $userToEdit->email .'" id="email">');

            $response->assertSee('<label for="password">Password:</label>');
            $response->assertSee('<input class="form-control" name="password" type="password" value="" id="password">');

            $response->assertSee('<input class="btn btn-primary" type="submit" value="Save">');
        }
    }

    private function generateDeleteUserTest(User $authUser = null)
    {
        $actingAs = !empty($authUser) && $authUser->is_admin ?
            $this->actingAs($authUser) : $this;

        $user = $this->generateUser();

        $response = $actingAs->call(
            'POST',
            route("users.destroy", $user->id),
            ['_token' => csrf_token(), '_method' => 'DELETE']
        );

        if(!empty($authUser) && $authUser->is_admin)
        {
            $deleted = User::where('id', '=', $user->id)->first();
            $this->assertEmpty($deleted);
            $this->assertEquals(302, $response->status());
            $response->assertRedirect(route("users.index"));
            $user->forceDelete();

            // Test deleting a non-existent user.
            $response = $actingAs->call(
                'POST',
                route("users.destroy", 123456789),
                ['_token' => csrf_token(), '_method' => 'DELETE']
            );
            $this->assertEquals(302, $response->status());
            $response->assertRedirect(route("users.index"));

            //Test deleting admin user.
            $response = $actingAs->call(
                'POST',
                route("users.destroy", User::adminUser()->id),
                ['_token' => csrf_token(), '_method' => 'DELETE']
            );
            $response->assertRedirect(route("users.index"));
        } else {
            $response->assertRedirect(route("login"));
        }
    }

    private function generateShowUserTest(User $userToView, User $viewingAsUser = null)
    {
        $actingAs = !empty($viewingAsUser) ? $this->actingAs($viewingAsUser) : $this;

        $response = $actingAs->call('GET', route("users.show", $userToView->id));
        $this->assertEquals(200, $response->status());
        $response->assertSee('<label for="name">Name:</label>');
        $response->assertSee('<p id="name">' . $userToView->name . '</p>');

        $otherFields = [
            '<label for="email">Email:</label>',
            '<p id="email">' . $userToView->email . '</p>',
            '<label for="password">Password:</label>',
            '<p id="password">********************</p>',
            '<label for="remember_token">Remember Token:</label>',
            '<p id="remember_token">********************</p>'
        ];

        if(!empty($viewingAsUser) && ($viewingAsUser->is_admin || $userToView->id === $viewingAsUser->id))
        {
            foreach ($otherFields as $field){
                $response->assertSee($field);
            }
        }

        if(!empty($viewingAsUser) && !$viewingAsUser->is_admin && $userToView->id !== $viewingAsUser->id)
        {
            foreach ($otherFields as $field){
                $response->assertDontSee($field);
            }
        }
    }

    private function usersDataTableIndexHeaderRow()
    {
        if(!empty(Auth::user()) && Auth::user()->is_admin){
            return '<tr><th  title="Name">Name</th><th  title="Email">Email</th><th  title="Email Verified At">Email Verified At</th><th  title="Is Admin">Is Admin</th><th  title="Action" width="120px">Action</th></tr>';
        }
        return '<tr><th  title="Name">Name</th><th  title="Action" width="120px">Action</th></tr>';

    }

    private function generateUser(): User
    {
        $user = $this->generateUserData();

        return User::create($user);
    }

    private function generateUserData():array
    {
        $user = factory(User::class)->raw();
        $user['is_admin'] = 0;

        return User::cleanMultipleInput($user);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
