<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function test_user_redirected_to_home_after_logging_in()
    {
        $user = User::adminUser();
        $response = $this->post(route('login'), [
            'email' => $user->email,
            'password' => Config::get('app.admin_password')
        ]);
        $this->assertAuthenticatedAs($user);
    }

    public function test_authenticated_user_can_visit_home()
    {
        $user = User::adminUser();
        $this->actingAs($user)
            ->get(url("/home"))
            ->assertStatus(200)
            ->assertViewIs("home")
            ->assertSeeText($user->name)
            ->assertSeeText('Profile')
            ->assertSeeText('Sign out')
            ->assertDontSeeText('Login')
            ->assertDontSeeText('Register');
        $this->assertAuthenticatedAs($user);
    }

    public function _test_unauthenticated_user_redirected_to_login()
    {
        $this->get(url("/home"))
            ->assertStatus(302)
            ->assertRedirect(route("login"));
    }


    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
