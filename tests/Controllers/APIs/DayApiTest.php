<?php namespace Tests\APIs;

use App\Http\Resources\DayResource;
use App\Models\Day;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Exception;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Tests\TestCase;

class DayApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware;

    private $days;
    private  $nonAdminUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->days = DayResource::collection(Day::all());
        $this->nonAdminUser = factory(User::class)->create();
    }

    public function test_read_days_index_as_admin_user()
    {
        $response = $this->generateIndexTest(200, $this->adminUser());

        if(!empty($response)) {
            foreach ($this->days as $day){
                $this->generateJsonFragmentTest($day, $response, $this->adminUser());
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_days_index_as_non_admin_user()
    {
        $response = $this->generateIndexTest(200, $this->nonAdminUser);

        if(!empty($response)) {
            foreach ($this->days as $day){
                $this->generateJsonFragmentTest($day, $response, $this->nonAdminUser);
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_days_index_as_guest_user()
    {
        $response = $this->generateIndexTest(200);

        if(!empty($response)) {
            foreach ($this->days as $day){
                $this->generateJsonFragmentTest($day, $response);
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_day()
    {
        $day = Day::first();

        $user = $this->adminUser();

        $actingAs = !empty($user) ? $this->actingAs($user) : $this;

        $this->response = $actingAs->json(
            'GET',
            'api/v1/days/'.$day->number
        );

        $this->assertApiResponse($day->toArray());
    }

    public function test_read_day_not_exists()
    {
        $this->get(route('api.days.show', 123456789))
            ->assertStatus(404);
    }

    public function tearDown(): void
    {
        try {
            User::where('is_admin', '=', false)->forceDelete();
            parent::tearDown();
        } catch (Throwable | BindingResolutionException $e) {
            echo("ERROR! Exception caused by: " . $e->getMessage());
        }
    }

    private function generateIndexTest(int $httpStatus, User $user = null)
    {
        $actingAs = !empty($user) ? $this->actingAs($user) : $this;

        $response = $actingAs->json('GET', 'api/v1/days');

        $response->assertStatus($httpStatus);

        $content = json_decode($response->getContent(), true);

        try {
            $this->assertArrayHasKey('data', $content);
        } catch (Exception | InvalidArgumentException $e) {
            echo ("Oops! An error occurred. Error info: " . $e->getMessage());
        } finally{
            return $response;
        }
    }

    private function generateJsonFragmentTest(DayResource $day, TestResponse $response, User $user = null)
    {
        $data = [
            "number"      => $day->number,
            "title"       => $day->title,
            "date"        => $day->date,
            "summary"     => $day->summary,
            "keywords"    => $day->keywords,
            "description" => $day->description,
            'created_at' => Carbon::parse($day->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::parse($day->updated_at)->format('Y-m-d H:i:s'),
        ];

        !empty($user) && $user->is_admin ? $data["id"] = $day->id : null;

        $response->assertJsonFragment(
            $data
        );
    }
}
