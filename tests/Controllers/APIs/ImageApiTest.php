<?php namespace Tests\APIs;

use App\Http\Resources\ImageResource;
use App\Http\Resources\TimezoneResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Exception;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Tests\TestCase;
use App\Models\Image;

class ImageApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    public function test_read_image_as_admin()
    {
        $image = Image::where('deleted_at', '=', null)
            ->first();

        $user = $this->adminUser();

        $actingAs = !empty($user) ? $this->actingAs($user) : $this;

        $this->response = $actingAs->json(
            'GET',
            '/api/v1/images/'.$image->id
        );

        $image = $image->toArray();

        unset($image['timezone_id']);

        $this->assertApiResponse($image);
    }

    public function test_read_deleted_image_as_admin()
    {
        $this->generateDeletedImageTest($this->adminUser());
    }

    public function test_read_deleted_image_as_guest_user()
    {
        $this->generateDeletedImageTest(null);
    }

    public function test_read_image_as_guest()
    {
        $image = Image::where('deleted_at', '=', null)
            ->first();

        $this->response = $this->json(
            'GET',
            '/api/v1/images/'.$image->id
        );

        $image = $image->toArray();

        unset($image['timezone_id']);
        unset($image['id']);
        unset($image['day_id']);
        unset($image['deleted_at']);

        $this->assertApiResponse($image);

    }

    public function test_read_image_not_exists()
    {
        $this->get(route('api.images.show', 123456789))
            ->assertStatus(404);
    }

    public function test_read_images_index_as_admin_user()
    {
        $response = $this->generateIndexTest(
            200,
            route('api.images.index'), $this->adminUser()
        );

        if(!empty($response)) {
            $images = ImageResource::collection(Image::all());
            foreach ($images as $image){

                $this->generateJsonFragmentTest($image, $response, $this->adminUser());
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_images_as_guest_user()
    {
        $response = $this->generateIndexTest(
            200,
            route('api.images.index')
        );

        if(!empty($response)) {
            $images = ImageResource::collection(Image::all());
            foreach ($images as $image){

                $this->generateJsonFragmentTest($image, $response);
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    private function generateJsonFragmentTest(ImageResource $responseImage, TestResponse $response, User $user = null)
    {
        $data = [
            'file_name' => $responseImage->file_name,
            'file_path' => $responseImage->file_path,
            'file_size' => $responseImage->file_size,
            'file_width' => $responseImage->file_width,
            'file_height' => $responseImage->file_height,
            'file_date_time' => $responseImage->file_date_time,
            'thumbnail_file_name' => $responseImage->thumbnail_file_name,
            'thumbnail_file_path' => $responseImage->thumbnail_file_path,
            'thumbnail_file_width' => $responseImage->thumbnail_file_width,
            'thumbnail_file_height' => $responseImage->thumbnail_file_height,
            'mime_type' => $responseImage->mime_type,
            'image_description' => $responseImage->image_description,
            'orientation' => $responseImage->orientation,
            'date_time' => Carbon::parse($responseImage->date_time)->format('Y-m-d H:i:s'),
            'gps_latitude_ref' => $responseImage->gps_latitude_ref,
            'gps_latitude' => $responseImage->gps_latitude,
            'gps_longitude_ref' => $responseImage->gps_longitude_ref,
            'gps_longitude' => $responseImage->gps_longitude,
            'gps_altitude_ref' => $responseImage->gps_altitude_ref,
            'gps_altitude' => $responseImage->gps_altitude,
            //'timezone' => $timezone,
            'created_at' => Carbon::parse($responseImage->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::parse($responseImage->updated_at)->format('Y-m-d H:i:s'),
        ];

        if(!empty($user) && $user->is_admin) {
            $data['id'] = $responseImage->id;
        }

        $response->assertJsonFragment(
            $data
        );

        $responseImages = json_decode($response->content(), true);
        $responseImages = $responseImages['data'];

        $id = $responseImage->id;

        $imageTimezone = current(
            array_filter(
                $responseImages,
                function ($e) use ($id) {
                    return !empty($e['id']) && $e['id'] == $id ? true: false;
                }
            )
        );

        $imageTimezone = $imageTimezone['timezone'];

        if(!empty($imageTimezone) && !empty($responseImage->timezone()->first())) {
            $requiredKeys = [
                'dst_offset',
                'raw_offset',
                'timezone_id',
                'timezone_name',
                'abbreviation'
            ];

            $containsAllKeys = false;
            if (count(array_intersect_key(array_flip($requiredKeys), $imageTimezone)) === count($requiredKeys)) {
                $containsAllKeys = true;
            }
            $this->assertTrue($containsAllKeys);
        }
    }

    private function generateIndexTest(int $httpStatus, string $url, User $user = null)
    {
        $actingAs = !empty($user) ? $this->actingAs($user) : $this;

        $response = $actingAs->json('GET', $url);

        $response->assertStatus($httpStatus);

        $content = $response->getContent();

        try {
            $this->assertArrayHasKey('data', json_decode($content, true));
        } catch (Exception | InvalidArgumentException $e) {
            echo ("Oops! An error occurred. Error info: " . $e->getMessage());
        } finally{
            return $response;
        }
    }

    private function generateDeletedImageTest(User $user = null)
    {
        $image = Image::where('deleted_at', '=', null)
            ->first();

        $image->delete();

        $actingAs = !empty($user) ? $this->actingAs($user) : $this;

        $this->response = $actingAs->json(
            'GET',
            '/api/v1/images/'.$image->id
        );

        $trashedImage = $image->toArray();

        unset($trashedImage['timezone_id']);

        $expected = 404;

        if(!empty($user) && $user->is_admin) {
            $expected = 200;
        }

        $this->assertEquals($expected, $this->response->status());

        $image->restore();
    }
}
