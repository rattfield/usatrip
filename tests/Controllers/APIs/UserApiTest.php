<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use PHPUnit\Framework\Exception;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Tests\TestCase;
use App\Models\User;

class UserApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    private $users;
    private $nonAdminUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->users = factory(User::class, 9)->create();
        $this->users = User::all();
        $this->nonAdminUser = User::where('is_admin', '=', false)->first();
    }

    public function test_read_user_as_admin_user()
    {
        $user = User::first();

        $response = $this->generateShowSingleUserTest(200, $user, $this->adminUser());

        if(!empty($response)) {
            $this->generateJsonFragmentTest($user, $response, $this->adminUser());
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_user_as_non_admin_user()
    {
        $user = User::first();

        $response = $this->generateShowSingleUserTest(200, $user, $this->nonAdminUser);

        if(!empty($response)) {
            $this->generateJsonFragmentTest($user, $response, $this->nonAdminUser);
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_user_as_guest_user()
    {
        $user = User::first();

        $response = $this->generateShowSingleUserTest(200, $user);

        if(!empty($response)) {
            $this->generateJsonFragmentTest($user, $response);
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_user_not_exists()
    {
        $this->get(route('api.users.show', 123456789))
            ->assertStatus(404);
    }

    public function test_read_users_index_as_admin_user()
    {
        $response = $this->generateIndexTest(200, $this->adminUser());

        if(!empty($response)) {

            foreach ($this->users as $u) {

                $this->generateJsonFragmentTest($u, $response, $this->adminUser());
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_users_index_as_non_admin_user()
    {
        $response = $this->generateIndexTest(200, $this->nonAdminUser);

        if(!empty($response)) {

            foreach ($this->users as $u) {

                $this->generateJsonFragmentTest($u, $response, $this->nonAdminUser);
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function test_read_users_index_as_guest_user()
    {
        $response = $this->generateIndexTest(200);

        if(!empty($response)) {
            foreach ($this->users as $u){
                $this->generateJsonFragmentTest($u, $response);
            }
        } else{
            echo("OOPS! 'response' is empty / null.");
        }
    }

    public function tearDown(): void
    {
        try {
            User::where('is_admin', '=', false)->forceDelete();
            parent::tearDown();
        } catch (Throwable | BindingResolutionException $e) {
            echo("ERROR! Exception caused by: " . $e->getMessage());
        }
    }

    private function generateShowSingleUserTest(int $httpStatus, User $testUser, User $authUser = null)
    {
        $actingAs = !empty($authUser) ? $this->actingAs($authUser) : $this;

        $response = $actingAs->get(route('api.users.show', $testUser->id));

        $response->assertStatus($httpStatus);

        $content = json_decode($response->getContent(), true);

        try {
            $this->assertArrayHasKey('data', $content);
        } catch (Exception | InvalidArgumentException $e) {
            echo ("Oops! An error occurred. Error info: " . $e->getMessage());
        } finally{
            return $response;
        }

    }

    private function generateIndexTest(int $httpStatus, User $authUser = null)
    {
        $actingAs = !empty($authUser) ? $this->actingAs($authUser) : $this;

        $response = $actingAs->json('GET', "api/v1/users");

        $response->assertStatus($httpStatus);

        $content = json_decode($response->getContent(), true);

        try {
            $this->assertArrayHasKey('data', $content);
        } catch (Exception | InvalidArgumentException $e) {
            echo ("Oops! An error occurred. Error info: " . $e->getMessage());
        } finally{
            return $response;
        }
    }

    private function generateJsonFragmentTest(User $user, TestResponse $response, User $authUser = null)
    {
        $data = [
            "name" =>  $user->name
        ];

        if(!empty($authUser) && $authUser->is_admin) {
            $data["id"] = $user->id;
            $data["email"] = $user->email;
            $data["created_at"] = $user->created_at;
            $data["updated_at"] = $user->updated_at;
        }

        $response->assertJsonFragment(
            $data
        );
    }
}
