<?php


namespace Tests\Controllers;

use App\Models\Day;
use App\Models\User;
use Tests\TestCase;
use Throwable;

class DayControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function test_index_view_as_guest()
    {
        $response = $this->call('GET', route("days.index"));

        $this->assertEquals(200, $response->status());
        $response->assertSeeText('Days');
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
        $response->assertSee($this->daysDataTableIndexHeaderRow());
    }

    public function test_index_view_as_admin_user()
    {
        $adminUser = User::adminUser();

        $response = $this->actingAs($adminUser)
            ->call('GET', route("days.index"));

        $this->assertEquals(200, $response->status());
        $response->assertSeeText('Days');
        $response->assertSeeText('Add New');
        $response->assertSeeText($adminUser->name);
        $response->assertSeeText('Profile');
        $response->assertSeeText('Sign out');
        $response->assertDontSeeText('Login');
        $response->assertDontSeeText('Register');
        $response->assertSee($this->daysDataTableIndexHeaderRow());
    }

    public function test_create_view_as_guest_user()
    {
        $this->call('GET', route("days.create"))
            ->assertRedirect(route("login"));
    }

    public function test_create_view_as_admin_user()
    {
        $adminUser = User::adminUser();

        $response = $this->actingAs($adminUser)
            ->call('GET', route("days.create"));

        $this->assertEquals(200, $response->status());

        $response->assertSeeText('Number:');
        $numberField = '<input class="form-control" name="number" type="number" id="number">';
        $response->assertSee($numberField);

        $response->assertSeeText('Title:');
        $titleField = '<input class="form-control" name="title" type="text" id="title">';
        $response->assertSee($titleField);

        $response->assertSeeText('Date:');
        $dateField = '<input class="form-control" id="date" name="date" type="date">';
        $response->assertSee($dateField);

        $response->assertSeeText('Summary:');
        $summaryField = '<input class="form-control" name="summary" type="text" id="summary">';
        $response->assertSee($summaryField);

        $response->assertSeeText('Keywords:');
        $keywordsField = '<input class="form-control" name="keywords" type="text" id="keywords">';
        $response->assertSee($keywordsField);

        $response->assertSeeText('Description:');
        $descriptionField = '<textarea class="form-control" name="description" cols="50" rows="10" id="description"></textarea>';
        $response->assertSee($descriptionField);

        $submitButton = '<input class="btn btn-primary" type="submit" value="Save">';
        $response->assertSee($submitButton);
    }

    public function test_show_day_view_as_guest_user()
    {
        $this->generateSingleDayShowTest();
    }

    public function test_show_day_view_as_admin_user()
    {
        $this->generateSingleDayShowTest(User::adminUser());
    }

    public function test_edit_day_view_as_guest_user()
    {
        $day = Day::first();
        $response = $this->call('GET', route("days.edit", $day->number));
        $response->assertRedirect(route("login"));
    }

    public function test_edit_day_view_as_admin_user()
    {
        $adminUser = User::adminUser();

        $day = Day::first();

        $response = $this->actingAs($adminUser)
            ->call('GET', route("days.edit", $day->number));
        $this->assertEquals(200, $response->status());

        $response->assertSeeText('Number:');
        $numberField = '<input class="form-control" name="number" type="number" value="' .
            $day->number . '" id="number">';
        $response->assertSee($numberField);

        $response->assertSeeText('Title:');
        $titleField = '<input class="form-control" name="title" type="text" value="' .
            $day->title .'" id="title">';
        $response->assertSee($titleField);

        $response->assertSeeText('Date:');
        $dateField = '<input class="form-control" id="date" name="date" type="date" value="' .
            $day->date . '">';
        $response->assertSee($dateField);

        $response->assertSeeText('Summary:');
        $summaryField = '<input class="form-control" name="summary" type="text" value="' .
            $day->summary . '" id="summary">';
        $response->assertSee($summaryField);

        $response->assertSeeText('Keywords:');
        $keywordsField = '<input class="form-control" name="keywords" type="text" value="' .
            $day->keywords . '" id="keywords">';
        $response->assertSee($keywordsField);

        $response->assertSeeText('Description:');
        $descriptionField = '<textarea class="form-control" name="description" cols="50"' .
            ' rows="10" id="description">' . $day->description . '</textarea>';
        $response->assertSee($descriptionField);

        $submitButton = '<input class="btn btn-primary" type="submit" value="Save">';
        $response->assertSee($submitButton);

        $response = $this->actingAs($adminUser)
            ->call('GET', route("days.edit", 123456789));
        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route("days.index"));
    }

    public function test_store_day_as_guest_user()
    {
        $dayData = $this->generateNewDayData();
        $response = $this->call('POST', route("days.store", $dayData));
        $response->assertRedirect(route("login"));
    }

    public function test_store_day_as_admin_user()
    {
        $dayData = $this->generateNewDayData();
        $response = $this->actingAs(User::adminUser())
            ->call('POST', route("days.store", $dayData));
        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route("days.index"));

        $day = Day::where('number', '=', $dayData['number'])
            ->where('date', '=', $dayData['date'])
            ->where('title', '=', $dayData['title'])->first();
        $this->assertNotEmpty($day);

        if(!empty($day)) {
            $day->forceDelete();
        }

    }

    public function test_update_day_as_guest_user()
    {
        $this->generateSingleDayUpdateTest();
    }

    public function test_update_day_as_admin_user()
    {
        $this->generateSingleDayUpdateTest(User::adminUser());
    }

    public function test_destroy_day_as_guest_user()
    {
        $day = Day::first();
        $response = $this->call(
            'POST',
            route("days.destroy", $day->number),
            ['_token' => csrf_token(), '_method' => 'DELETE']
        );
        $response->assertRedirect(route("login"));
    }

    public function test_destroy_day_as_admin_user()
    {
        //Test deleting a day which exists in database.
        $day = $this->generateNewDayData();
        $newDay = Day::create($day);
        $response = $this->actingAs(User::adminUser())
            ->call(
                'POST',
                route("days.destroy", $newDay->number),
                ['_method' => 'DELETE']
            );
        $deleted = Day::where('number', '=', $newDay->number)->first();
        $this->assertEmpty($deleted);
        $newDay->forceDelete();
        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route("days.index"));

        // Test deleting a non-existant day.
        $response = $this->actingAs(User::adminUser())
            ->call(
                'POST',
                route("days.destroy", 123456789),
                ['_token' => csrf_token(), '_method' => 'DELETE']
            );
        $this->assertEquals(302, $response->status());
        $response->assertRedirect(route("days.index"));

    }

    protected function tearDown(): void
    {
        try {
            parent::tearDown();
        } catch (Throwable $e) {
            echo("ERROR! Exception caused by: " . $e->getMessage());
        }
    }

    private function generateSingleDayShowTest(User $user = null)
    {
        $actingAs = !empty($user) ? $this->actingAs($user) : $this;

        $day = Day::first();

        $response = $actingAs->call('GET', route("days.show", $day->number));
        $this->assertEquals(200, $response->getStatusCode());
        $response->assertViewHas('day', $day);
        $response->assertSeeText($day->title);
        $response->assertSeeText($day->date);
        $response->assertSeeText($day->summary);
        $response->assertSeeText($day->keywords);
        $response->assertSeeText($day->description);

        if(empty($user)) {
            $response->assertSeeText("Login");
            $response->assertSee(route("login"));
            $response->assertSeeText("Register");
            $response->assertSee(route("register"));
        } else {
            $response->assertSeeText($user->name);
            $response->assertSeeText("Profile");
            $response->assertSeeText("Sign out");
            $response->assertSee(route("logout"));
        }

        $response = $actingAs->call('GET', route("days.show", 123456789));
        $response->assertStatus(404);
    }

    private function generateSingleDayUpdateTest(User $user = null)
    {
        $actingAs = !empty($user) ? $this->actingAs($user) : $this;

        //Test for day which currently exists in DB.
        $testDayData = $this->generateNewDayData();
        $testDay = Day::create($testDayData);
        $updateData = $this->generateNewDayData();
        $updateData['number'] = $testDay->number;
        $updateData['id'] = $testDay->id;
        //dd(route("days.update", $testDay->number));
        $response = $actingAs->patch(
            route("days.update", $testDay->number),
            $updateData
        );

        if (empty($user))
        {
            $response->assertRedirect(route("login"));
        }
        else{
            $this->assertEquals(302, $response->status());
            $response->assertRedirect(route('days.show', $testDay->number));

            //Test for non-existent day.
            $response = $actingAs->patch(
                route("days.update", 123456789),
                $updateData
            );
            $response->assertStatus(302);
            $response->assertRedirect(route('days.index'));
        }

        if(!empty($testDay))
        {
            $testDay->forceDelete();
        }

    }

    private function daysDataTableIndexHeaderRow()
    {
        return '"columns":[{"name":"number","data":"number","title":"Number","orderable":true,"searchable":true},{"name":"title","data":"title","title":"Title","orderable":true,"searchable":true},{"name":"date","data":"date","title":"Date","orderable":true,"searchable":true},{"name":"summary","data":"summary","title":"Summary","orderable":true,"searchable":true},{"name":"keywords","data":"keywords","title":"Keywords","orderable":true,"searchable":true},{"name":"description","data":"description","title":"Description","orderable":true,"searchable":true},{"defaultContent":"","data":"action","name":"action","title":"Action","render":null,"orderable":false,"searchable":false,"width":"120px"}]';
    }

}
