<?php

namespace Tests;

use App\Models\Day;
use Carbon\Carbon;
use Orchestra\Testbench\TestCase as Orchestra;

abstract class TestCase extends Orchestra
{
    use CreatesApplication;

    protected function generateNewDayData()
    {
        $lastDay = Day::orderBy('id', 'desc')->limit(1)->first();
        $dayNumber = $lastDay->number + 1;
        $day = factory(Day::class)->raw(
            [
                "number" => $dayNumber,
                "date" => Carbon::parse($lastDay->date)->addDays(1)->format('Y-m-d'),
                "title" => "Day " . $dayNumber . '.'
            ]
        );
        return $day;
    }
}
