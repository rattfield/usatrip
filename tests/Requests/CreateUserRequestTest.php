<?php

namespace Tests\Feature;

use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use Faker\Factory;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;
use Tests\RequestTest;
use Tests\TestCase;

class CreateUserRequestTest extends TestCase
{
    /**
     * @var CreateUserRequest
     */
    private $rules;

    private $attributes;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var User
     */
    private $nonAdminUser;

    protected function setUp():void
    {
        parent::setUp();

        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->validator = app()->get('validator');

        $this->rules = (new CreateUserRequest())->rules();

        $this->attributes = [
            'name' => $faker->firstName . ' ' . $faker->lastName,
            'email' => $faker->safeEmail,
            'password' => Str::random(25),
            'is_admin' => false
        ];

        factory(User::class)->create(
            ['name' => 'testuser', 'is_admin' => false]
        );

        $this->nonAdminUser = User::where('name', '=', 'testuser')
            ->where('is_admin', '=', false)
            ->first();
    }

    public function test_authorise_method_as_admin_user()
    {
        $this->generateAuthRequestTest(User::adminUser(), true);
    }

    public function test_valid_name_field()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $chars = [' ', '', '-', '_'];

        $this->assertTrue($this->validateField('name', "BobJones"));

        foreach($chars as $c){
            $this->assertTrue($this->validateField('name', "Bob".$c."Jones"));
            $this->assertTrue($this->validateField('name', "B".$c."o".$c."b".$c."J".$c."o".$c."n".$c."e".$c."s"));
            $this->assertTrue($this->validateField('name', "123".$c."456".$c."789"));
            $this->assertTrue($this->validateField('name', "1".$c."2".$c."3".$c."4".$c."5".$c."6".$c."7".$c."8".$c."9"));
            $this->assertTrue($this->validateField('name', "123 456".$c."789"));
            $this->assertTrue($this->validateField('name', "123".$c."456 789"));
            $this->assertTrue($this->validateField('name', "Agent".$c."99"));
            $this->assertTrue($this->validateField('name', "A".$c."g".$c."e".$c."n".$c."t".$c."9".$c."9"));
            $this->assertTrue($this->validateField('name', "99".$c."Red".$c."Balloons"));
            $this->assertTrue($this->validateField('name', "99".$c."Red Balloons"));
            $this->assertTrue($this->validateField('name', "99 Red".$c."Balloons"));
            $this->assertTrue($this->validateField('name', "9".$c."9".$c."R".$c."e".$c."d".$c."B".$c."a".$c."l".$c."l".$c."o".$c."o".$c."n".$c."s"));
        }

        $this->assertTrue($this->validateField('name', "123_456-789"));
        $this->assertTrue($this->validateField('name', "123-456_789"));

        $startEndChars = [' ','-', '_',"'",'"',"\"","/",'=','?','<','>',',','?','!','@','#','$','%','^','&','*','(',')','~','`'];

        for($i = 0; $i < count($startEndChars); $i++) {
            $c = $startEndChars[$i];

            if($c != $startEndChars[0] && $c != $startEndChars[1] && $c != $startEndChars[1]) {
                $this->assertFalse($this->validateField('name', $c . "Bob Jones"));
            }

            if($c != $startEndChars[0]) {
                $this->assertFalse($this->validateField('name', "Bob Jones" . $c));
                $this->assertFalse($this->validateField('name', "Bo" . $c));
            }
            $this->assertFalse($this->validateField('name', $c . "Bo"));
            $this->assertFalse($this->validateField('name', $c));
        }

        $this->assertFalse($this->validateField('name', 123456789));
        $this->assertFalse($this->validateField('name', 123));
        $this->assertFalse($this->validateField('name', 12));
        $this->assertFalse($this->validateField('name', ""));
        $this->assertFalse($this->validateField('name', "   "));
        $this->assertFalse($this->validateField('name', $faker->text(300)));
        $this->assertFalse($this->validateField('name', null));

    }

    public function test_valid_email_field()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->assertTrue($this->validateField('email', "BobJones@example.com"));
        $this->assertTrue($this->validateField('email', $faker->email));
        $this->assertTrue($this->validateField('email', $faker->safeEmail));
        $this->assertTrue($this->validateField('email', $faker->freeEmail));
        $this->assertTrue($this->validateField('email', "email@example.com"));
        $this->assertTrue($this->validateField('email', "firstname.lastname@example.com"));
        $this->assertTrue($this->validateField('email', "email@subdomain.example.com"));
        $this->assertTrue($this->validateField('email', "firstname+lastname@example.com"));
        $this->assertTrue($this->validateField('email', "\"email\"@example.com"));
        $this->assertTrue($this->validateField('email', "1234567890@example.com"));
        $this->assertTrue($this->validateField('email', "email@example-one.com"));
        $this->assertTrue($this->validateField('email', "_______@example.com"));
        $this->assertTrue($this->validateField('email', "email@example.name"));
        $this->assertTrue($this->validateField('email', "email@example.museum"));
        $this->assertTrue($this->validateField('email', "email@example.co.jp"));
        $this->assertTrue($this->validateField('email', "firstname-lastname@example.com"));
        $this->assertTrue($this->validateField('email', "email@example.web"));

        $this->assertFalse($this->validateField('email', User::adminUser()->email));

        $this->assertFalse($this->validateField('email', "BobJones@example"));
        $this->assertFalse($this->validateField('email', "plainaddress"));
        $this->assertFalse($this->validateField('email', "#@%^%#$@#$@#.com"));
        $this->assertFalse($this->validateField('email', "@example.com"));
        $this->assertFalse($this->validateField('email', "Joe Smith <email@example.com>"));
        $this->assertFalse($this->validateField('email', "email.example.com"));
        $this->assertFalse($this->validateField('email', "email@example@example.com"));
        $this->assertFalse($this->validateField('email', ".email@example.com"));
        $this->assertFalse($this->validateField('email', "email.@example.com"));
        $this->assertFalse($this->validateField('email', "email..email@example.com"));
        $this->assertFalse($this->validateField('email', "あいうえお@example.com"));
        $this->assertFalse($this->validateField('email', "email@example.com (Joe Smith)"));
        $this->assertFalse($this->validateField('email', "email@example"));
        $this->assertFalse($this->validateField('email', "email@-example.com"));
        $this->assertFalse($this->validateField('email', "email@111.222.333.44444"));
        $this->assertFalse($this->validateField('email', "email@example..com"));
        $this->assertFalse($this->validateField('email', "Abc..123@example.com"));
        $this->assertFalse($this->validateField('email', "“(),:;<>[\]@example.com"));
        $this->assertFalse($this->validateField('email', "just\"not\"right@example.com"));
        $this->assertFalse($this->validateField('email', "this\ is\"really\"not\allowed@example.com"));
        $this->assertFalse($this->validateField('email', "example.com"));
        $this->assertFalse($this->validateField('email', "A@b@c@domain.com"));
        $this->assertFalse($this->validateField('email', "a”b(c)d,e:f;gi[j\k]l@domain.com"));
        $this->assertFalse($this->validateField('email', "abc”test”email@domain.com"));
        $this->assertFalse($this->validateField('email', "abc is”not\valid@domain.com"));
        $this->assertFalse($this->validateField('email', "abc\ is\”not\valid@domain.com"));
        $this->assertFalse($this->validateField('email', "test@domain..com"));
        $this->assertFalse($this->validateField('email', " email@example.com"));
        $this->assertFalse($this->validateField('email', "email@example.com "));
        $this->assertFalse($this->validateField('email', "1234567890123456789012345678901234567890123456789012345678901234+x@example.com"));
        $this->assertFalse($this->validateField('email', "abc..def@mail.com"));
        $this->assertFalse($this->validateField('email', ".abc@mail.com"));
        $this->assertFalse($this->validateField('email', "email@123.123.123.123"));
        $this->assertFalse($this->validateField('email', "much.”more\ unusual”@example.com"));
        $this->assertFalse($this->validateField('email', "very.unusual.”@”.unusual.com@example.com"));
        $this->assertFalse($this->validateField('email', "very.”(),:;<>[]”.VERY.”very@\\ \"very”.unusual@strange.example.com"));
    }

    public function test_valid_password_field()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->assertFalse($this->validateField('password', ""));
        $this->assertFalse($this->validateField('password', null));
        $this->assertFalse($this->validateField('password', "          "));
        $this->assertFalse($this->validateField('password', "password"));
        $this->assertFalse($this->validateField('password', "passwordxx"));
        $this->assertFalse($this->validateField('password', "passwordx9"));
        $this->assertFalse($this->validateField('password', "9passwordx"));
        $this->assertFalse($this->validateField('password', "PASSWORDXX"));
        $this->assertFalse($this->validateField('password', "PASSWORDX9"));
        $this->assertFalse($this->validateField('password', "PaSsWoRdXx"));
        $this->assertFalse($this->validateField('password', "PaSsWoRdX9"));
        $this->assertFalse($this->validateField('password', "9PaSsWoRdX"));
        $this->assertFalse($this->validateField('password', "password!@"));
        $this->assertFalse($this->validateField('password', "password!9"));
        $this->assertFalse($this->validateField('password', "!@#$%^&*()"));
        $this->assertFalse($this->validateField('password', $faker->password(9, 9)));
        $this->assertFalse($this->validateField('password', $faker->password(21, 21)));
        $this->assertFalse($this->validateField('password', Str::random(21)));

        $passwordTestCount = 18;
        for($i = 1; $i <= 18; $i++)
        {
            if($i <= 9) {
                $this->randomPasswordTest(false, 21, 21, 1, 1, 1, $i);
            }
            $this->randomPasswordTest(false, 21, 21, 1, 1, $i, 1);
            $this->randomPasswordTest(false, 21, 21, 1, $i, 1, 1);
            $this->randomPasswordTest(false, 21, 21, $i, 1, 1, 1);
            $passwordTestCount++;
        }

        for($minLength = 10; $minLength <= 20; $minLength++)
        {
            for($maxLength = 10; $maxLength <= 20; $maxLength ++)
            {
                $minLowerVal = floor(($maxLength/$minLength) + 1);
                $minUpperVal = floor(($maxLength/$minLength) + 1);
                $minSymbolVal = floor(($maxLength/$minLength) + 1);
                $minCharVal = floor(($maxLength/$minLength) + 1);

                for($minLower = 1; $minLower <= $minLowerVal; $minLower++)
                {
                    for($minUpper = 1; $minUpper <= $minUpperVal; $minUpper++)
                    {
                        for($minSymbol = 1; $minSymbol <= $minSymbolVal; $minSymbol++)
                        {
                            for($minChar = 1; $minChar <= $minCharVal; $minChar++)
                            {
                                $this->randomPasswordTest(true, $minLength, $maxLength, $minLower, $minUpper, $minSymbol, $minChar);
                                $passwordTestCount++;
                            }
                        }
                    }
                }
            }
        }

        echo "\n\nTotal number of password assertions done: " . $passwordTestCount . '.';
    }

    protected function tearDown(): void
    {
        User::where('is_admin', '=', false)->forceDelete();
        parent::tearDown();
    }

    private function generateAuthRequestTest(User $user, bool $expected = false)
    {
        if(empty($user)) {
            echo("An existing user must be passed in as first parameter");
        }

        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->actingAs($user);

        $request = new CreateUserRequest();
        $request->setContainer($this->app);

        $attributes = [
            'name' => $faker->firstName . ' ' . $faker->lastName,
            'email' => $faker->safeEmail,
            'password' => Str::random(25),
            'is_admin' => false
        ];

        $request->initialize([], $attributes);
        $this->app->instance('request', $request);
        $authorized = $request->authorize();

        $this->assertEquals($expected, $authorized);
    }

    protected function getFieldValidator($field, $value)
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        );
    }

    protected function validateField($field, $value)
    {
        return $this->getFieldValidator($field, $value)->passes();
    }

    private function randomPasswordTest(bool $assert, $minLength = 10, $maxLength = 20, $minLower = 1, $minUpper = 1, $minSymbol = 1, $minDigits = 1)
    {
        $randomPassword = $this->generateRandomPassword($minLength, $maxLength, $minLower, $minUpper, $minSymbol, $minDigits);

        if($assert) {
            $this->assertTrue(
                $this->validateField(
                    'password',
                    $randomPassword
                )
            );
        } else{
            $this->assertFalse(
                $this->validateField(
                    'password',
                    $randomPassword
                )
            );
        }
    }

    private function generateRandomPassword($minLength = 10, $maxLength = 20, $minLower = 1, $minUpper = 1, $minSymbol = 1, $minDigits = 1)
    {
        $password = "";

        $passwordLength = rand($minLength, $maxLength);

        $lowerChars = str_split("abcdefghijklmanopqrstuvwxyz");
        $upperChars = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        $symbols = str_split("`~!@#$%^&*()_-=+[]{}\|;:'\",<.>/?");
        $digits = str_split("0123456789");

        shuffle($lowerChars);
        shuffle($upperChars);
        shuffle($symbols);
        shuffle($digits);

        $count = floor($maxLength / 4);
        $remainder = $maxLength % 4;

        try {

            $chars = [];

            for($i = 0; $i < $count; $i++){
                if(strlen($password) != $maxLength) {
                    $lower = $lowerChars[random_int($minLower, count($lowerChars) - 1)];
                    $upper = $upperChars[random_int($minUpper, count($upperChars) - 1)];
                    $symbol = $symbols[random_int($minSymbol, count($symbols) - 1)];
                    $digit = $digits[random_int($minDigits, count($digits) - 1)];

                    $chars[0] = $lower;
                    $chars[1] = $upper;
                    $chars[2] = $symbol;
                    $chars[3] = $digit;

                    $password .= $lower;
                    $password .= $upper;
                    $password .= $symbol;
                    $password .= $digit;
                }
            }

            for($i = 0; $i < $remainder; $i++){
                if(strlen($password) != $maxLength) {
                    $password .= $chars[random_int(0, 3)];
                }
            }

            return substr($password, 0, $passwordLength);
        } catch (\Exception $e) {
            return false;
        }

    }
}
