<?php

namespace Tests\Requests;

use App\Http\Requests\CreateDayRequest;
use App\Models\Day;
use App\Models\User;
use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;
use Faker\Factory;
use Mockery;
use Tests\TestCase;

class CreateDayRequestTest extends TestCase
{
    /**
     * @var CreateDayRequest
     */
    private $rules;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var Day
     */
    private $existingDay;

    protected function setUp():void
    {
        parent::setUp();

        $this->validator = app()->get('validator');
        $this->rules = (new CreateDayRequest())->rules();

        $this->existingDay = Day::first();
    }

    public function test_valid_number_field()
    {
        $this->assertTrue($this->validateField('number', 12));
        $this->assertFalse($this->validateField('number', $this->existingDay->number));
        $this->assertFalse($this->validateField('number', 4000)); // more than max days.
        $this->assertFalse($this->validateField('number', null));
    }

    public function test_valid_title_field()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->assertTrue($this->validateField('title', "Day 12."));
        $this->assertTrue($this->validateField('title', "Day Twelve."));
        $this->assertFalse($this->validateField('title', "Day"));
        $this->assertFalse($this->validateField('title', "     "));
        $this->assertFalse($this->validateField('title', 123456789));
        $this->assertFalse($this->validateField('title', null));
        $this->assertFalse($this->validateField('title', $this->existingDay->title));
        $this->assertFalse($this->validateField('title', $faker->text(1000)));
    }

    public function test_valid_date_field()
    {
        $date = Carbon::parse('2019-03-23')->format('Y-m-d');

        $this->assertTrue($this->validateField('date', $date));
        $this->assertTrue($this->validateField('date', Carbon::now()->format('Y-m-d')));
        $this->assertFalse($this->validateField('date', "1899-12-31"));
        $this->assertFalse($this->validateField('date', $this->existingDay->date));
        $this->assertFalse($this->validateField('date', "ihf83478gf80gf8"));
        $this->assertFalse($this->validateField('date', "20190323"));
        $this->assertFalse($this->validateField('date', "23-03-2019"));
        $this->assertFalse($this->validateField('date', "23032019"));
        $this->assertFalse($this->validateField('date', 20190323));
        $this->assertFalse($this->validateField('date', "        "));
        $this->assertFalse($this->validateField('date', null));
    }

    public function test_valid_summary_field()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->assertTrue($this->validateField('summary', $faker->text(255)));
        $this->assertTrue($this->validateField('summary', null));
        $this->assertTrue($this->validateField('summary', "Foo"));
        $this->assertFalse($this->validateField('summary', $faker->text(1000)));
    }

    public function test_valid_keywords_field()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->assertTrue($this->validateField('keywords', implode(',', $faker->words(10))));
        $this->assertTrue($this->validateField('keywords', null));
        $this->assertTrue($this->validateField('keywords', "Foo"));
        $this->assertFalse($this->validateField('keywords', $faker->text(255)));
        $this->assertFalse($this->validateField('keywords', implode(',', $faker->words(100))));
    }

    public function test_valid_description_field()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->assertTrue($this->validateField('description', $faker->text(500)));
        $this->assertTrue($this->validateField('description', "Foo"));
        $this->assertTrue($this->validateField('description', null));
        $this->assertTrue($this->validateField('description', $faker->text(65535)));
        $this->assertFalse($this->validateField('description', $faker->text(70000)));
    }

    public function test_all_fields_valid()
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $validateNumber = $this->validateField('number', 12);
        $validateTitle = $this->validateField('title', "Day 12.");
        $validateDate = $this->validateField('date', "2019-03-23");
        $validateSummary = $this->validateField('summary', $faker->text(255));
        $validateKeywords = $this->validateField('keywords', implode(',', $faker->words(10)));
        $validateDesc = $this->validateField('description', $faker->text(500));

        $this->assertTrue(
            $validateNumber &&
            $validateTitle &&
            $validateDate &&
            $validateSummary &&
            $validateKeywords &&
            $validateDesc
        );
    }

    public function test_authorise_method_as_admin_user()
    {
        $this->generateAuthRequestTest(User::adminUser(), true);
    }

    public function test_authorise_method_as_non_admin_user()
    {
        factory(User::class)->create();
        $nonAdminUser = User::where('is_admin', '=', false)->first();

        $this->generateAuthRequestTest($nonAdminUser, false);
        User::where('is_admin', '=', false)->forceDelete();
    }

    protected function tearDown():void
    {
        parent::tearDown();
    }

    private function generateAuthRequestTest(User $user, bool $expected = false)
    {
        if(empty($user)) {
            echo("An existing user must be passed in as first parameter");
        }

        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        $this->actingAs($user);

        $request = new CreateDayRequest();
        $request->setContainer($this->app);

        $attributes = [
            'number' => 12,
            'title' => "Day 12.",
            'date' => "2019-03-23",
            'summary' => $faker->text(255),
            'keywords' => implode(',', $faker->words(10)),
            'description' => $faker->text(500)
        ];

        $request->initialize([], $attributes);
        $this->app->instance('request', $request);
        $authorized = $request->authorize();

        $this->assertEquals($expected, $authorized);
    }

    protected function getFieldValidator($field, $value)
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        );
    }

    protected function validateField($field, $value)
    {
        return $this->getFieldValidator($field, $value)->passes();
    }
}
