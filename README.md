# usatrip

![PHP Version](https://img.shields.io/badge/php-7.3%2B-green.svg) ![PHP Version](https://img.shields.io/badge/php-7.2%2B-green.svg)
[![codecov](https://codecov.io/bb/rattfield/usatrip/branch/master/graph/badge.svg)](https://codecov.io/bb/rattfield/usatrip)
[![CircleCI](https://circleci.com/bb/rattfield/usatrip/tree/master.svg?style=svg)](https://circleci.com/bb/rattfield/usatrip/tree/master)

A Laravel-based project, which uses Google Maps and KML files for showing where I went in the US. Also, GPS data from stored images will show where they were taken on Google Maps.

[link-author]: https://github.com/rattfieldnz
