<?php

namespace App\Console\Commands;

use App\Constants;
use App\Models\Day;
use App\Models\Image;
use App\Utilities\DiskUtility;
use App\Utilities\GpsUtility;
use App\Utilities\ImageUtility;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Exception\NotWritableException;
use InvalidArgumentException;

/**
 * Class ProcessImagesCommand
 *
 * Processes images for a single day, or for all
 * existing days in the database.
 *
 * @package App\Console\Commands
 */
class ProcessImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:process {dayNumber}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process existing images and link them to their days. ' .
                             'Enter "all" to process all days and their images.';

    private $storagePath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->storagePath = storage_path() . "/app/";
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Exception
     */
    public function handle(): void
    {
        $dayNumber = $this->argument('dayNumber');

        if ($dayNumber == 'all') {

            $startTime = Carbon::now();

            $this->processAllDays();

            $this->showProcessingInfo($startTime);

        } else {

            if (intval($dayNumber) < 0) {
                $this->error("Please enter an integer value for a day number.");
            }

            $day = Day::where('number', '=', $dayNumber)->first();

            if (empty($day)) {
                $this->error("Day number does not match day record in database");
            }

            $startTime = Carbon::now();

            try {
                $this->processSingleDay($day);
                $this->showProcessingInfo($startTime);
            } catch (Exception $e) {
                $this->error(
                    "Error! Images for Day {$day->number} could not be processed. 
                             Error info: {$e->getTraceAsString()}"
                );
            }
        }
    }

    /**
     * Process all days currently in the database.
     *
     * @return bool Return true if successful, false if not.
     *
     * @throws Exception
     */
    private function processAllDays(): bool
    {
        $days = Day::all();

        $success = false;

        $daysProgressBar = $this->output->createProgressBar(count($days));
        $daysProgressBar->setFormat(
            "Day " . Constants::CMD_PROGRESS_BAR_FORMAT
        );
        $daysProgressBar->start();

        foreach ($days as $day) {
            $success = $this->processSingleDay($day);
            $daysProgressBar->advance();
        }

        $daysProgressBar->finish();

        return $success;
    }

    /**
     * Processes all images for a single Day instance.
     *
     * @param Day $day Day instance to be processed.
     *
     * @return bool Return true if successful, false if not.
     *
     * @throws Exception
     */
    private function processSingleDay(Day $day): bool
    {
        if (empty($day)) {
            $this->error("Day record does not exist in database");
            return false;
        }

        $success = false;

        if (Storage::disk('public')->exists("images/day" . $day->number)) {

            $imageFiles = Storage::files(DiskUtility::getImagesStoragePath() . "day" . $day->number);

            $imagesProgressBar = $this->output->createProgressBar(count($imageFiles));
            $imagesProgressBar->setFormat(
                "Images " . Constants::CMD_PROGRESS_BAR_FORMAT
            );
            $this->info("\n");

            $imagesProgressBar->start();

            foreach ($imageFiles as $image) {

                $success = $this->processSingleImage($day, $image);

                $imagesProgressBar->advance();
            }

            $imagesProgressBar->finish();
        }

        return $success;

    }

    /**
     * Process a single image for a single Day instance.
     *
     * @param  Day    $day       Day instance to be processed.
     * @param  string $imageFile Path to image file for processing.
     * @return bool If image was successfully processed.
     * @throws Exception
     */
    private function processSingleImage(Day $day, string $imageFile)
    {
        if (empty($day)) {
            $this->error("Day record does not exist in database");
            return false;
        }

        if (!Storage::exists($imageFile)) {
            $this->error(
                "The image file does not exist in " .
                $this->storagePath . DiskUtility::getImagesStoragePath() .
                "day{$day->number} directory."
            );
            return false;
        }

        try {

            ImageUtility::saveImageToDatabase($day, $imageFile);

            $success = true;

        }
        catch (InvalidArgumentException $e) {

            ImageUtility::removeThumbNailDirectory($day);

            DB::rollBack();
            $success = false;
            $this->error(
                "Error! Image data could not be parsed properly in Image model. 
                Error info: {$e->getTraceAsString()}"
            );

        }
        catch (QueryException $e) {

            ImageUtility::removeThumbNailDirectory($day);

            DB::rollBack();
            $success = false;
            $this->error(
                "Error! Image could not be saved into database.
                Error info: {$e->getTraceAsString()}"
            );

        }
        catch (NotWritableException $e) {

            ImageUtility::removeThumbNailDirectory($day);

            DB::rollBack();
            $success = false;
            $this->error(
                "Error! Thumbnail image could not be stored in 
                day{$day->number}/thumbnails. Error info: 
                {$e->getTraceAsString()}"
            );
        }
        catch (Exception $e) {

            ImageUtility::removeThumbNailDirectory($day);

            DB::rollBack();
            $success = false;
            $this->error(
                "Error! Image could not processed. Error info: " . $e->getTraceAsString()
            );
        }

        return $success;
    }

    /**
     * Outputs image processing stats from $startTime to current.
     *
     * @param Carbon $startTime Carbon instance representing start time.
     *
     * @return void
     */
    private function showProcessingInfo(Carbon $startTime): void
    {
        try {
            $finishTime = Carbon::now();

            $this->info("\n\nStart time: " . $startTime->format('Y-m-d H:i:s') . '.');
            $this->info("Finish time: " . $finishTime->format('Y-m-d H:i:s') . '.');

            $durationInSeconds = $finishTime->diffInSeconds($startTime);
            $formatted = CarbonInterval::seconds($durationInSeconds)->cascade()->forHumans();

            $this->info("Duration of image processing: {$formatted}.\n");
        } catch(Exception $e){
            $this->error("\$startTime is in the wrong format. Further info: {$e->getTraceAsString()}.");
        }
    }
}
