<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->mergeWhen(
                !empty(Auth::user()) && Auth::user()->is_admin, [
                'id' => $this->id,
                ]
            ),
            'name' => $this->name,
            $this->mergeWhen(
                !empty(Auth::user()) && Auth::user()->is_admin, [
                'email' => $this->email,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at
                ]
            ),
        ];
    }
}
