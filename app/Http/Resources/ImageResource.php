<?php

namespace App\Http\Resources;

use App\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ImageResource extends JsonResource
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request)
    {
        $timezone = !empty($this->timezone()->first()) ?
            new TimezoneResource($this->timezone()->first()) :
            null;

        $data = [
            'file_name' => $this->file_name,
            'file_path' => $this->file_path,
            'file_size' => $this->file_size,
            'file_width' => $this->file_width,
            'file_height' => $this->file_height,
            'file_date_time' => $this->file_date_time,
            'thumbnail_file_name' => $this->thumbnail_file_name,
            'thumbnail_file_path' => $this->thumbnail_file_path,
            'thumbnail_file_width' => $this->thumbnail_file_width,
            'thumbnail_file_height' => $this->thumbnail_file_height,
            'mime_type' => $this->mime_type,
            'image_description' => $this->image_description,
            'orientation' => $this->orientation,
            'date_time' => Carbon::parse($this->date_time)->format('Y-m-d H:i:s'),
            'gps_latitude_ref' => $this->gps_latitude_ref,
            'gps_latitude' => $this->gps_latitude,
            'gps_longitude_ref' => $this->gps_longitude_ref,
            'gps_longitude' => $this->gps_longitude,
            'gps_altitude_ref' => $this->gps_altitude_ref,
            'gps_altitude' => $this->gps_altitude,
            'timezone' => $timezone,
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::parse($this->updated_at)->format('Y-m-d H:i:s')
        ];

        if(!empty(Auth::user()) && Auth::user()->is_admin) {
            $data['id'] = $this->id;
            $data['day_id'] = $this->day_id;
            $data['deleted_at'] = empty($this->deleted_at) ?
                $this->deleted_at :
                Carbon::parse($this->deleted_at)->format('Y-m-d H:i:s');
        }

        return $data;
    }
}
