<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class TimezoneResource extends JsonResource
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'dst_offset' => $this->dst_offset,
            'raw_offset' => $this->raw_offset,
            'timezone_id' => $this->timezone_id,
            'timezone_name' => $this->timezone_name,
            'abbreviation' => $this->abbreviation
        ];

        if(!empty(Auth::user()) && Auth::user()->is_admin) {
            //$data['id'] = $this->id;
        }

        return $data;
    }
}
