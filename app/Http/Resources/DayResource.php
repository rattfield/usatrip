<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class DayResource extends JsonResource
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->mergeWhen(
                !empty(Auth::user()) && Auth::user()->is_admin, [
                'id' => $this->id,
                ]
            ),
            'number' => $this->number,
            'title' => $this->title,
            'date' => $this->date,
            'summary' => $this->summary,
            'keywords' => $this->keywords,
            'description' => $this->description,
            'slug' => $this->slug,
            'images' => ImageResource::collection($this->images()->get()),
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::parse($this->updated_at)->format('Y-m-d H:i:s'),
            $this->mergeWhen(
                !empty(Auth::user()) && Auth::user()->is_admin, [
                    'deleted_at' => empty($this->deleted_at) ?
                        $this->deleted_at :
                        Carbon::parse($this->deleted_at)->format('Y-m-d H:i:s'),
                ]
            ),
        ];
    }
}
