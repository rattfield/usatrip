<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;

class UserController extends AppBaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of users.
     *
     * @param  UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     * @throws Exception
     */
    public function store(CreateUserRequest $request)
    {
        if (Auth::user()->is_admin) {
            try {
                DB::beginTransaction();
                $this->userRepository->create($request->all());
                DB::commit();
                Flash::success('User saved successfully.');
            } catch (Exception $e){
                DB::rollBack();
                Flash::error('Sorry! An error occurred while saving the new user. Error information: ' . $e->getMessage() . '.');
            }
        } else{
            Flash::error('Sorry! You are unable to create new users.');
        }

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            abort(404, "User not found.");
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(Auth::user()->is_admin || Auth::user()->id == $id) {
            $user = $this->userRepository->find($id);

            if (empty($user)) {
                Flash::error('User not found');

                return redirect(route('users.index'));
            }

            return view('users.edit')->with('user', $user);
        } else{
            Flash::error('User cannot be edited, or you have insufficient privileges,');

            return redirect(route('users.index'));
        }
    }

    /**
     * Update the specified User in storage.
     *
     * @param int               $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     * @throws MassAssignmentException
     * @throws ModelNotFoundException
     * @throws Exception
     */
    public function update($id, UpdateUserRequest $request)
    {
        if(Auth::user()->id !== $id && !Auth::user()->is_admin) {
            abort(403, "Only admin user can update other users' records.");
        } else {

            $user = $this->userRepository->find($id);

            if (empty($user)) {
                Flash::error('User not found');
            } else {
                try {
                    DB::beginTransaction();
                    $this->userRepository->update($request->all(), $id);
                    DB::commit();
                    Flash::success('User with id "' . $user->id . '" updated successfully.');
                } catch (Exception $e) {
                    DB::rollBack();
                    Flash::error('Sorry! An error occurred while updating the user. Error information: ' . $e->getMessage() . '.');
                }
            }

        }
        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws ModelNotFoundException
     * @throws Exception
     */
    public function destroy($id)
    {
        if(Auth::user()->id != $id && !Auth::user()->is_admin) {
            Flash::error("Only admin user can update other users' records.");

            return redirect(route('users.index'));
        } else {
            $user = $this->userRepository->find($id);

            if (empty($user)) {
                Flash::error('User not found');

                return redirect(route('users.index'));
            }

            if ($user->is_admin) {
                Flash::error('Admin user cannot be deleted.');

                return redirect(route('users.index'));
            }

            try {
                DB::beginTransaction();
                $this->userRepository->delete($id);
                DB::commit();
                Flash::success('User with id "' . $id . '" deleted successfully.');
            } catch (Exception $e){
                DB::rollBack();
                Flash::error('Sorry! An error occurred while deleting the user. Error information: ' . $e->getMessage() . '.');
            }

            return redirect(route('users.index'));
        }
    }
}
