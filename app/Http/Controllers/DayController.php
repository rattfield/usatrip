<?php

namespace App\Http\Controllers;

use App\DataTables\DayDataTable;
use App\Http\Requests\CreateDayRequest;
use App\Http\Requests\UpdateDayRequest;
use App\Repositories\DayRepository;
use Flash;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Auth;
use Response;

class DayController extends AppBaseController
{
    /**
     * @var DayRepository
     */
    private $dayRepository;

    public function __construct(DayRepository $dayRepo)
    {
        $this->dayRepository = $dayRepo;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of days.
     *
     * @param  DayDataTable $dayDataTable
     * @return Response
     */
    public function index(DayDataTable $dayDataTable)
    {
        return $dayDataTable->render('days.index');
    }

    /**
     * Show the form for creating a new Day.
     *
     * @return Response
     */
    public function create()
    {
        return view('days.create');
    }

    /**
     * Store a newly created Day in storage.
     *
     * @param CreateDayRequest $request
     *
     * @return Response
     */
    public function store(CreateDayRequest $request)
    {
        $input = $request->all();

        $day = $this->dayRepository->create($input);

        Flash::success('Day saved successfully.');

        return redirect(route('days.index'));
    }

    /**
     * Display the specified Day.
     *
     * @param int $number
     *
     * @return Response
     */
    public function show($number)
    {
        $day = $this->dayRepository->find($number);

        if (empty($day)) {
            abort(404, "Day not found.");
        }

        return view('days.show')->with('day', $day);
    }

    /**
     * Show the form for editing the specified Day.
     *
     * @param  $number
     * @return Response
     */
    public function edit($number)
    {
        $day = $this->dayRepository->find($number);

        if (empty($day)) {
            Flash::error('Day not found');

            return redirect(route('days.index'));
        }

        return view('days.edit')->with('day', $day);
    }

    /**
     * Update the specified Day in storage.
     *
     * @param $number
     * @param UpdateDayRequest $request
     *
     * @return Response
     * @throws MassAssignmentException
     */
    public function update($number, UpdateDayRequest $request)
    {
        $day = $this->dayRepository->find($number);

        if (empty($day)) {
            Flash::error('Day not found');

            return redirect(route('days.index'));
        }

        $day = $this->dayRepository->update($request->all(), $number);

        Flash::success('Day updated successfully.');

        return redirect(route('days.show', $day->number));
    }

    /**
     * Remove the specified Day from storage.
     *
     * @param  $number
     * @return Response
     * @throws \Exception
     */
    public function destroy($number)
    {
        $day = $this->dayRepository->find($number);

        if (empty($day)) {
            Flash::error('Day not found');

            return redirect(route('days.index'));
        }

        $this->dayRepository->delete($number);

        Flash::success('Day deleted successfully.');

        return redirect(route('days.index'));
    }
}
