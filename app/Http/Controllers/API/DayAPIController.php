<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\DayResource;
use App\Models\Day;
use App\Repositories\DayRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DayController
 *
 * @package App\Http\Controllers\API
 */

class DayAPIController extends AppBaseController
{
    /**
     * @var DayRepository
     */
    private $dayRepository;

    public function __construct(DayRepository $dayRepo)
    {
        $this->dayRepository = $dayRepo;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the Day.
     * GET|HEAD /days
     *
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $days = $this->dayRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $days = DayResource::collection($days);

        return $this->sendResponse($days->toArray($request), 'Days retrieved successfully');
    }

    /**
     * Display the specified Day.
     * GET|HEAD /days/{id}
     *
     * @param int $number
     *
     * @return Response
     */
    public function show($number)
    {
        /**
         * @var Day $day
         */
        $day = $this->dayRepository->find($number);

        if (empty($day)) {
            return $this->sendError('Day not found');
        }

        $day = new DayResource($day);

        return $this->sendResponse($day, 'Day retrieved successfully');
    }
}
