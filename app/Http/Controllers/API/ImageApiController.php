<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\AppBaseController;
use App\Http\Resources\ImageResource;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class ImageApiController extends AppBaseController
{
    private $imageRepo;

    public function __construct(ImageRepository $imageRepo)
    {
        $this->imageRepo = $imageRepo;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of Images.
     * GET|HEAD /images
     *
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $images = $this->imageRepo->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $images = ImageResource::collection($images);

        return $this->sendResponse($images->toArray($request), 'Images retrieved successfully');
    }

    /**
     * Display the specified Image.
     * GET|HEAD /images/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $image = Image::find($id);

        if(!empty(Auth::user() && Auth::user()->is_admin)) {
            $image = Image::withTrashed()->find($id);
        }

        if (empty($image) && empty(Auth::user())) {
            return $this->sendError('Image not found');
        }

        $image = new ImageResource($image);

        return $this->sendResponse($image, 'Image retrieved successfully');
    }
}
