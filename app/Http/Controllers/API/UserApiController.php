<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\UserCollectionResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

class UserApiController extends AppBaseController
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of users.
     * GET|HEAD /days
     *
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $users = UserResource::collection($users);

        return $this->sendResponse(
            $users->toArray($request), 'Users retrieved successfully'
        );
    }

    /**
     * Display the specified Day.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = new UserResource($user);

        return $this->sendResponse($user, 'User retrieved successfully');
    }


}
