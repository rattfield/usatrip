<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Day;
use Illuminate\Support\Facades\Auth;

class UpdateDayRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (!empty(Auth::user()) && Auth::user()->is_admin);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Day::$rules;

        $data = $this->request->all();

        $rules['number'] = $rules['number'] . ', '. intval($data['id']);
        $rules['title'] = $rules['title'] . ', '. $data['id'];
        $rules['date'] = $rules['date'] . ', '. $data['id'];

        return $rules;
    }
}
