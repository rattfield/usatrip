<?php

namespace App\Models;

use Eloquent;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Stevebauman\Purify\Facades\Purify;

/**
 * App\Models\User
 *
 * @property      int $id
 * @property      string $name
 * @property      string $email
 * @property      Carbon|null $email_verified_at
 * @property      string $password
 * @property      string|null $remember_token
 * @property      Carbon|null $created_at
 * @property      Carbon|null $updated_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method        static Builder|User newModelQuery()
 * @method        static Builder|User newQuery()
 * @method        static Builder|User query()
 * @method        static Builder|User whereCreatedAt($value)
 * @method        static Builder|User whereEmail($value)
 * @method        static Builder|User whereEmailVerifiedAt($value)
 * @method        static Builder|User whereId($value)
 * @method        static Builder|User whereName($value)
 * @method        static Builder|User wherePassword($value)
 * @method        static Builder|User whereRememberToken($value)
 * @method        static Builder|User whereUpdatedAt($value)
 * @mixin         Eloquent
 * @property      bool $is_admin
 * @method        static Builder|User whereIsAdmin($value)
 */
class User extends Authenticatable implements CanResetPassword
{
    use Notifiable;

    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'email_verified_at', 'password', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'is_admin' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * Password must have at least 1 upper and lower-case character,
     * at least 1 number, and at least 1 symbol.
     *
     * @var array
     */
    public static $rules = [
        'name' => "required|string|min:3|max:255|regex:/^([a-zA-Z0-9]{1,}[\s\-_]{0,})+([^-_\/\\=\\\"\'\?<>,!@#$%^&*\(\)\+\=\:\;~`]$)+$/",
        'email' =>'required|email:filter|max:255|unique:users,email',
        'password' => [
            'required',
            'min:10',
            'max:20',
            'regex:/^(?=.*[a-z|A-Z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/'
        ],
        'is_admin' => 'sometimes|boolean'
    ];

    public function setIsAdminAttribute($value)
    {
        $this->attributes['is_admin'] = isset($value) ? intval($value) : 0;
    }

    public function setPasswordAttribute($pass)
    {

        $this->attributes['password'] = bcrypt($pass);

    }

    public function isAdmin()
    {
        return $this->attributes['is_admin'] ? "Yes" : "No";
    }

    /**
     * Sanitize input for database insertion.
     *
     * @param  $value Data to be sanitized.
     * @return string|null Data after being sanitized.
     */
    public static function cleanInput($value)
    {
        $config = [
            'HTML.Doctype' => 'HTML 4.01 Transitional',
            'HTML.Allowed' => '',
            'AutoFormat.AutoParagraph' => false,
            'AutoFormat.Linkify' => false,
        ];

        return isset($value) ? Purify::clean($value, $config) : null;
    }

    /**
     * Sanitize data input from array.
     *
     * @param  array $data Data to be sanitized.
     * @return array|null Data after being sanitized.
     */
    public static function cleanMultipleInput(array $data)
    {
        if(is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = isset($value) ? self::cleanInput($value) : null;
            }
            return $data;
        }
        return null;
    }

    public static function adminUser()
    {
        return self::where('is_admin', '=', true)
            ->first();
    }
}
