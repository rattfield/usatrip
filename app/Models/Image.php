<?php

namespace App\Models;

use App\Utilities\GpsUtility;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 *
 * @package       App\Models
 * @version       October 30, 2019, 9:49 am UTC
 * @property      Day day
 * @property      string file_name
 * @property      string file_path
 * @property      integer file_date_time
 * @property      integer file_size
 * @property      integer file_width
 * @property      integer file_height
 * @property      string mime_type
 * @property      string image_description
 * @property      integer orientation
 * @property      string|Carbon date_time
 * @property      string|Carbon date_time_original
 * @property      string|Carbon date_time_digitized
 * @property      integer exif_image_width
 * @property      integer exif_image_height
 * @property      string gps_latitude_ref
 * @property      number gps_latitude
 * @property      string gps_longitude_ref
 * @property      number gps_longitude
 * @property      string gps_altitude_ref
 * @property      integer gps_altitude
 * @property      integer day_id
 * @property      int $id
 * @property      string $thumbnail_file_name
 * @property      string $thumbnail_file_path
 * @property      int $thumbnail_file_width
 * @property      int $thumbnail_file_height
 * @property      \Illuminate\Support\Carbon|null $created_at
 * @property      \Illuminate\Support\Carbon|null $updated_at
 * @property      \Illuminate\Support\Carbon|null $deleted_at
 * @property-read TimeZone $timezone
 * @method        static bool|null forceDelete()
 * @method        static Builder|Image newModelQuery()
 * @method        static Builder|Image newQuery()
 * @method        static \Illuminate\Database\Query\Builder|Image onlyTrashed()
 * @method        static Builder|Image query()
 * @method        static bool|null restore()
 * @method        static Builder|Image whereCreatedAt($value)
 * @method        static Builder|Image whereDateTime($value)
 * @method        static Builder|Image whereDateTimeDigitized($value)
 * @method        static Builder|Image whereDateTimeOriginal($value)
 * @method        static Builder|Image whereDayId($value)
 * @method        static Builder|Image whereDeletedAt($value)
 * @method        static Builder|Image whereExifImageHeight($value)
 * @method        static Builder|Image whereExifImageWidth($value)
 * @method        static Builder|Image whereFileDateTime($value)
 * @method        static Builder|Image whereFileHeight($value)
 * @method        static Builder|Image whereFileName($value)
 * @method        static Builder|Image whereFilePath($value)
 * @method        static Builder|Image whereFileSize($value)
 * @method        static Builder|Image whereFileWidth($value)
 * @method        static Builder|Image whereGpsAltitude($value)
 * @method        static Builder|Image whereGpsAltitudeRef($value)
 * @method        static Builder|Image whereGpsLatitude($value)
 * @method        static Builder|Image whereGpsLatitudeRef($value)
 * @method        static Builder|Image whereGpsLongitude($value)
 * @method        static Builder|Image whereGpsLongitudeRef($value)
 * @method        static Builder|Image whereId($value)
 * @method        static Builder|Image whereImageDescription($value)
 * @method        static Builder|Image whereMimeType($value)
 * @method        static Builder|Image whereOrientation($value)
 * @method        static Builder|Image whereThumbnailFileHeight($value)
 * @method        static Builder|Image whereThumbnailFileName($value)
 * @method        static Builder|Image whereThumbnailFilePath($value)
 * @method        static Builder|Image whereThumbnailFileWidth($value)
 * @method        static Builder|Image whereUpdatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|Image withTrashed()
 * @method        static \Illuminate\Database\Query\Builder|Image withoutTrashed()
 * @mixin         \Eloquent
 */
class Image extends BaseModel
{
    use SoftDeletes;

    public $table = 'images';

    protected $primaryKey = 'id';

    protected $dates = [
        self::CREATED_AT,
        self::UPDATED_AT,
        'deleted_at',
        'date_time',
    ];

    public $fillable = [
        'file_name',
        'file_path',
        'file_date_time',
        'file_size',
        'file_width',
        'file_height',
        'thumbnail_file_name',
        'thumbnail_file_path',
        'thumbnail_file_width',
        'thumbnail_file_height',
        'mime_type',
        'image_description',
        'orientation',
        'date_time',
        'gps_latitude_ref',
        'gps_latitude',
        'gps_longitude_ref',
        'gps_longitude',
        'gps_altitude_ref',
        'gps_altitude',
        'day_id',
        'timezone_id',
        'deleted_at'
    ];

    public $guarded = ['id', 'day_id', 'timezone_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'file_name' => 'string',
        'file_path' => 'string',
        'file_size' => 'integer',
        'file_width' => 'integer',
        'file_height' => 'integer',
        'file_date_time' => 'integer',
        'thumbnail_file_name' => 'string',
        'thumbnail_file_path' => 'string',
        'thumbnail_file_width' => 'integer',
        'thumbnail_file_height' => 'integer',
        'mime_type' => 'string',
        'image_description' => 'string',
        'orientation' => 'integer',
        'date_time' => 'datetime',
        'gps_latitude_ref' => 'string',
        'gps_latitude' => 'float',
        'gps_longitude_ref' => 'string',
        'gps_longitude' => 'float',
        'gps_altitude_ref' => 'string',
        'gps_altitude' => 'integer',
        'day_id' => 'integer',
        'timezone_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'file_name' => 'required|string|unique:images,file_name',
        'file_path' => 'required|string|unique:images,file_path',
        'file_size' => 'required|integer',
        'file_width' => 'required|integer',
        'file_height' => 'required|integer',
        'file_date_time' => 'nullable|integer',
        'thumbnail_file_name' => 'required|string|unique:images,thumbnail_file_name',
        'thumbnail_file_path' => 'required|string|unique:images,thumbnail_file_path',
        'thumbnail_file_width' => 'required|integer',
        'thumbnail_file_height' => 'required|integer',
        'image_description' => 'nullable|string|max:65535',
        'mime_type' => 'nullable|string',
        'orientation' => 'nullable|integer',
        'date_time' => 'required|date_format:Y-m-d H:i:s',
        'gps_latitude_ref' => 'nullable|string',
        'gps_latitude' => 'nullable|float',
        'gps_longitude_ref' => 'nullable|string',
        'gps_longitude' => 'nullable|float',
        'gps_altitude_ref' => 'nullable|string',
        'gps_altitude' => 'nullable|integer',
        'day_id' => 'required|integer|exists:days,id',
        'timezone_id' => 'nullable|integer|exists:timezones,id'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function setDateTimeAttribute($value)
    {
        $this->setDateAttribute('date_time', $value);
    }

    public function setGpsLatitudeAttribute($value)
    {
        if(!empty($value) && is_array($value)) {
            $latitudeRef = $this->attributes['gps_latitude_ref'];
            $this->attributes['gps_latitude'] =
                GpsUtility::calculateLatitude($latitudeRef, $value);
        } else {
            $this->attributes['gps_latitude'] = $value;
        }
    }

    public function setGpsLongitudeAttribute($value)
    {
        if(!empty($value) && is_array($value)) {
            $longitudeRef = $this->attributes['gps_longitude_ref'];
            $this->attributes['gps_longitude'] =
                GpsUtility::calculateLongitude($longitudeRef, $value);
        } else {
            $this->attributes['gps_longitude'] = $value;
        }
    }

    public function setGpsAltitudeAttribute($value)
    {
        if(!empty($value)) {
            $this->attributes['gps_altitude'] = eval('return ' . $value . ';');
        } else {
            $this->attributes['gps_altitude'] = null;
        }
    }

    public function setMimeTypeAttribute($value)
    {
        if(!empty($value)) {
            $this->attributes['mime_type'] = json_decode($value, false);
        } else {
            $this->attributes['mime_type'] = null;
        }
    }

    private function setDateAttribute($attributeName, $value)
    {
        $dateTime = null;
        if(!empty($value)) {
            list($date, $time) = explode(' ', $value);
            $date = str_replace(':', '-', $date);
            $dateTime = $date . ' ' . $time;
        }
        if(!empty($attributeName) && !empty($this->attributes && !empty($dateTime))) {
            try{
                $this->attributes[$attributeName] = Carbon::parse($dateTime)->format('Y-m-d H:i:s');
            } catch(\Exception $e){
                $this->attributes[$attributeName] = null;
            }
        }
    }

    /**
     * @return BelongsTo
     **/
    public function day()
    {
        return $this->belongsTo(Day::class, 'day_id');
    }

    /**
     * @return BelongsTo
     */
    public function timezone()
    {
        return $this->belongsTo(TimeZone::class, 'timezone_id');
    }
}
