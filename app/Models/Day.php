<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Day
 *
 * @package       App\Models
 * @version       October 8, 2019, 8:17 am UTC
 * @property      int $id
 * @property      int $number
 * @property      string $title
 * @property      string $date
 * @property      string|null $summary
 * @property      string|null $keywords
 * @property      string|null $description
 * @property      \Illuminate\Support\Carbon|null $created_at
 * @property      \Illuminate\Support\Carbon|null $updated_at
 * @property      \Illuminate\Support\Carbon|null $deleted_at
 * @method        static bool|null forceDelete()
 * @method        static Builder|Day newModelQuery()
 * @method        static Builder|Day newQuery()
 * @method        static \Illuminate\Database\Query\Builder|Day onlyTrashed()
 * @method        static Builder|Day query()
 * @method        static bool|null restore()
 * @method        static Builder|Day whereCreatedAt($value)
 * @method        static Builder|Day whereDate($value)
 * @method        static Builder|Day whereDeletedAt($value)
 * @method        static Builder|Day whereDescription($value)
 * @method        static Builder|Day whereId($value)
 * @method        static Builder|Day whereKeywords($value)
 * @method        static Builder|Day whereNumber($value)
 * @method        static Builder|Day whereSummary($value)
 * @method        static Builder|Day whereTitle($value)
 * @method        static Builder|Day whereUpdatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|Day withTrashed()
 * @method        static \Illuminate\Database\Query\Builder|Day withoutTrashed()
 * @mixin         \Eloquent
 * @property      string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TimeZone[] $timezones
 * @property-read int|null $timezones_count
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\Day findSimilarSlugs($attribute, $config, $slug)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereSlug($value)
 */
class Day extends BaseModel
{
    use SoftDeletes;
    use Sluggable;

    /**
     * @var string
     */
    public $table = 'days';

    protected $dates = ['deleted_at', 'date'];

    public $fillable = [
        'number',
        'title',
        'date',
        'summary',
        'keywords',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'number' => 'integer',
        'title' => 'string',
        'date' => 'datetime:Y-m-d',
        'summary' => 'string',
        'keywords' => 'string',
        'description' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //max for 'number' approx 10 years.
        'number' => 'required|integer|min:0|max:3700|unique:days,number',
        'title' => 'required|string|min:5|max:255|unique:days,title',
        'date' => 'required|date_format:Y-m-d|after:1899-12-31|unique:days,date',
        'summary' => 'nullable|string|min:0|max:255',
        'keywords' => [
            'nullable',
            'string',
            'min:0',
            'max:255',
            'regex:/^([-A-Za-z0-9]{1,},\s{0,1})*([-A-Za-z0-9]*)*([^,\s]|[^,$]|[^,\s$])$/'
        ],
        'description' => 'nullable|string|min:0|max:65535'
    ];

    /**
     * @param  $value
     * @return string
     */
    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * @param $value
     */
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'number'
            ]
        ];
    }

    /**
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    /**
     * @return HasManyThrough
     */
    public function timezones()
    {
        return $this->hasManyThrough(TimeZone::class, Image::class);
    }
}
