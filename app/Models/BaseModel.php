<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Stevebauman\Purify\Facades\Purify;

abstract class BaseModel extends  Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Sanitize input for database insertion.
     *
     * @param  $value Data to be sanitized.
     * @return string|null Data after being sanitized.
     */
    public static function cleanInput($value)
    {
        $config = [
            'HTML.Doctype' => 'HTML 4.01 Transitional',
            'HTML.Allowed' => '',
            'AutoFormat.AutoParagraph' => false,
            'AutoFormat.Linkify' => false,
        ];

        return !empty($value) ? Purify::clean($value, $config) : null;
    }

    /**
     * Sanitize data input from array.
     *
     * @param  array $data Data to be sanitized.
     * @return array|null Data after being sanitized.
     */
    public static function cleanMultipleInput(array $data)
    {
        if(is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = !empty($value) ? self::cleanInput($value) : null;
            }
            return $data;
        }
        return null;
    }
}
