<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TimeZone
 *
 * @package       App\Models
 * @version       November 11, 2019, 2:45 am UTC
 * @property      integer dst_offset
 * @property      integer raw_offset
 * @property      string timezone_id
 * @property      string timezone_name
 * @property      string abbreviation
 * @property      int $id
 * @property      \Illuminate\Support\Carbon|null $created_at
 * @property      \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @method        static bool|null forceDelete()
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone newModelQuery()
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone newQuery()
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\TimeZone onlyTrashed()
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone query()
 * @method        static bool|null restore()
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereAbbreviation($value)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereCreatedAt($value)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereDstOffset($value)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereId($value)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereRawOffset($value)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereTimezoneId($value)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereTimezoneName($value)
 * @method        static \Illuminate\Database\Eloquent\Builder|\App\Models\TimeZone whereUpdatedAt($value)
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\TimeZone withTrashed()
 * @method        static \Illuminate\Database\Query\Builder|\App\Models\TimeZone withoutTrashed()
 * @mixin         \Eloquent
 */
class TimeZone extends BaseModel
{
    use SoftDeletes;

    public $table = 'timezones';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'dst_offset',
        'raw_offset',
        'timezone_id',
        'timezone_name',
        'abbreviation'
    ];

    public $guarded = ['id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'dst_offset' => 'integer',
        'raw_offset' => 'integer',
        'timezone_id' => 'string',
        'timezone_name' => 'string',
        'abbreviation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'dst_offset' => 'required|integer|min:0|max:3600',
        'raw_offset' => 'required|integer|min:-86400|max:86400',
        'timezone_id' => 'required|string|min:10|max:255|unique:timezones,timezone_id,timezone_name,abbreviation',
        'timezone_name' => 'required|string|min:10|max:255|unique:timezones,timezone_id,timezone_name,abbreviation',
        'abbreviation' => 'required|string|min:3|max:10|unique:timezones,timezone_id,timezone_name,abbreviation'
    ];

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
