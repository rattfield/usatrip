<?php

namespace App\DataTables;

use App\Models\Image;
use App\Utilities\ImageUtility;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ImageDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param  mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->addColumn(
            'image', function (Image $image) {
                return ImageUtility::renderImageThumbHtml($image);
            }
        )->addColumn(
            'day', function (Image $image) {
                return ImageUtility::renderImageDayLinkHtml($image);
            }
        )->addColumn(
            'latitude', function (Image $image) {
                return !empty($image->gps_latitude) ? floatval($image->gps_latitude): null;
            }
        )->addColumn(
            'longitude', function (Image $image) {
                return !empty($image->gps_longitude) ? floatval($image->gps_longitude): null;
            }
        );

        if(!empty(Auth::user()) && Auth::user()->is_admin) {
            $dataTable->addColumn('action', 'images.datatables_actions');
        }

        $dataTable->rawColumns(['image', 'day'], true);

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param  Image $model
     * @return Builder
     */
    public function query(Image $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $builder = $this->builder()
            ->columns(
                [
                    'image' => ['title' => 'Image'],
                    'date_time' => ['title' => 'Date'],
                    'latitude' => ['title' => 'Latitude'],
                    'longitude' => ['title' => 'Longitude'],
                    'gps_altitude' => ['title' => 'Altitude (meters above sea level)'],
                    'day' => ['title' => "Day"]
                ]
            )
            ->minifiedAjax();
        if(!empty(Auth::user()) && Auth::user()->is_admin) {
            $builder->addAction(['width' => '120px', 'printable' => false]);
        }

            $builder->parameters(
                [
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                ]
            );

            return $builder;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'image',
            'date_time',
            'latitude',
            'longitude',
            'gps_altitude',
            'day'
        ];
    }

    public static function scripts()
    {
        return '<script type="text/javascript">
            (function(window,$){
                window.LaravelDataTables=window.LaravelDataTables||{};
                window.LaravelDataTables["dataTableBuilder"]=$("#dataTableBuilder").DataTable({
                    serverSide:true,
                    processing:false,
                    ajax:{url:"",type:"GET",data: function(data) {
                        for (var i = 0, len = data.columns.length; i < len; i++) {
                            if (!data.columns[i].search.value) delete data.columns[i].search;
                            if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                            if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                            if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                            
                        }
                        delete data.search.regex;
                    }},
                    columns:[
                        {
                            name: "image",
                            data: "image",
                            title: "Image",
                            orderable: true,
                            searchable: true
                        },
                        {
                            name: "date_time",
                            data: "date_time",
                            title: "Date",
                            orderable: true,
                            searchable: true
                        },
                        {
                            name: "latitude",
                            data: "latitude",
                            title: "Latitude",
                            type: "number",
                            orderable: true,
                            searchable: true
                        },
                        {
                            name: "longitude",
                            data: "longitude",
                            title: "Longitude",
                            type: "number",
                            orderable: true,
                            searchable: true
                        },
                        {
                            name: "gps_altitude",
                            data: "gps_altitude",
                            title: "Altitude (meters above sea level)", 
                            type: "number",
                            orderable: true,
                            searchable: true,
                        },
                        {
                            name: "day",
                            data: "day",
                            title: "Day",
                            orderable: true,
                            searchable: true
                        }
                    ],
                    dom:"Bfrtip",
                    stateSave:true,
                    order:[[0,"desc"]],
                    buttons:[
                        {
                            extend: "create",
                            className: "btn btn-default btn-sm no-corner"
                        },
                        {
                            extend: "export",
                            className: "btn btn-default btn-sm no-corner"
                        },
                        {
                            extend: "print",
                            className: "btn btn-default btn-sm no-corner"
                        },
                        {
                            extend: "reset",
                            className: "btn btn-default btn-sm no-corner"
                        },
                        {
                            extend: "reload",
                            className: "btn btn-default btn-sm no-corner"
                        }
                    ]
                }
            );
        })(window,jQuery);
        </script>';
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'imagesdatatable_' . time();
    }
}
