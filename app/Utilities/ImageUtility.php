<?php


namespace App\Utilities;

use App\Constants;
use App\Models\Day;
use App\Models\Image;
use App\Models\TimeZone;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image as InterventionImage;
use Intervention\Image\Facades\Image as InterventionImageFacade;

/**
 * Class ImageUtility
 *
 * @package App\Utilities
 */
class ImageUtility
{
    /**
     * Generate data for Image to be saved in database.
     *
     * @param Day    $day       Day for which image data to be generated.
     * @param string $imageFile Path to image stored on current server.
     *
     * @return array|null Data to be saved.
     * @throws \Exception
     */
    public static function generateImageData(Day $day, string $imageFile)
    {
        if (!empty($day) && Storage::exists($imageFile)) {

            $imagePath = storage_path('app') . Constants::DIR_SEPARATOR . $imageFile;

            if (mime_content_type($imagePath) == Constants::MIME_TYPE_JPEG) {

                $imageData = InterventionImageFacade::make($imagePath);
                $imageFileName = pathinfo($imagePath)['basename'];

                $filePath = DiskUtility::getPublicImagesPath() . "day" .
                    $day->number . Constants::DIR_SEPARATOR . $imageFileName;

                $imageExif = $imageData->exif();
                $imageWidth = $imageData->width();
                $imageHeight = $imageData->height();
                $imageMimeType = $imageExif['MimeType'];
                $imageSizeBytes = $imageData->filesize();

                $thumbNailImage = self::createThumbnail(
                    $day, $imagePath,
                    Constants::DEFAULT_THUMBNAIL_WIDTH,
                    Constants::DEFAULT_THUMBNAIL_HEIGHT
                );

                $thumbNailPath = self::createThumbnailPublicFilePath($day, $thumbNailImage);

                $latitude = null;
                $longitude = null;
                $timezoneId = null;

                if(isset($imageExif['GPSLatitudeRef']) && isset($imageExif['GPSLatitude'])
                    && isset($imageExif['GPSLongitudeRef']) && isset($imageExif['GPSLongitude'])
                ) {

                    $latitude = GpsUtility::calculateLatitude($imageExif['GPSLatitudeRef'], $imageExif['GPSLatitude']);
                    $longitude = GpsUtility::calculateLongitude($imageExif['GPSLongitudeRef'], $imageExif['GPSLongitude']);

                    $gpsUtility = new GpsUtility();

                    $timezone = $gpsUtility->setLatitude($imageExif["GPSLatitudeRef"], $imageExif['GPSLatitude'])
                        ->setLongitude($imageExif["GPSLongitudeRef"], $imageExif['GPSLongitude'])
                        ->setDateTime(new Carbon($imageExif['DateTime']))
                        ->getTimezone();

                    if(!empty($timezone)) {
                        $timezoneId = TimeZone::where("timezone_id", '=', $timezone["timeZoneId"])
                            ->where("timezone_name", '=', $timezone["timeZoneName"])
                            ->first();

                        $timezoneId = !empty($timezoneId) ? $timezoneId->id : null;
                    }
                }

                $dataToSave = [
                    'file_name' => $imageFileName,
                    'file_path' => $filePath,
                    'file_size' => $imageSizeBytes,
                    'file_width' => $imageWidth,
                    'file_height' => $imageHeight,
                    'file_date_time' => isset($imageExif['FileDateTime']) ?
                        $imageExif['FileDateTime'] : null,
                    'thumbnail_file_name' => $thumbNailImage->basename,
                    'thumbnail_file_path' => $thumbNailPath,
                    'thumbnail_file_width' => $thumbNailImage->width(),
                    'thumbnail_file_height' => $thumbNailImage->height(),
                    'mime_type' => isset($imageMimeType) ?
                        $imageMimeType : Constants::MIME_TYPE_JPEG,
                    'image_description' => isset($imageExif['ImageDescription']) ?
                        $imageExif['ImageDescription'] : null,
                    'orientation' => isset($imageExif['Orientation']) ?
                        $imageExif['Orientation'] : null,
                    'date_time' => isset($imageExif['DateTime']) ?
                        $imageExif['DateTime'] : null,
                    'gps_latitude_ref' => isset($imageExif['GPSLatitudeRef']) ?
                        $imageExif['GPSLatitudeRef'] : null,
                    'gps_latitude' => $latitude,
                    'gps_longitude_ref' => isset($imageExif['GPSLongitudeRef']) ?
                        $imageExif['GPSLongitudeRef'] : null,
                    'gps_longitude' => $longitude,
                    'gps_altitude_ref' => isset($imageExif['GPSAltitudeRef']) ?
                        $imageExif['GPSAltitudeRef'] : null,
                    'gps_altitude' => isset($imageExif['GPSAltitude']) ?
                        $imageExif['GPSAltitude'] : null,
                    'day_id' => $day->id,
                    'timezone_id' => $timezoneId
                ];

                return Image::cleanMultipleInput($dataToSave);
            }
        }
        return null;
    }

    /**
     * Save single image associated with a Day instance to the database.
     *
     * @param Day    $day       Day for which image to be saved.
     * @param string $imageFile Path to image stored on current server.
     *
     * @return bool
     *
     * @throws \Exception Throws generic exception if another type of error is generated.
     */
    public static function saveImageToDatabase(Day $day, string $imageFile): bool
    {
        if (!empty($day) && Storage::exists($imageFile)) {

            $dataToSave = self::generateImageData($day, $imageFile);

            if (!empty($dataToSave)) {

                if (array_key_exists('file_name', $dataToSave)
                    && array_key_exists('file_path', $dataToSave)
                    && array_key_exists('day_id', $dataToSave)
                ) {
                    $checkImage = Image::where('file_name', '=', $dataToSave['file_name'])
                        ->where('file_path', '=', $dataToSave['file_path'])
                        ->where('day_id', '=', $dataToSave['day_id'])
                        ->first();

                    if (empty($checkImage)) {
                        DB::beginTransaction();
                        $imageToSave = new Image($dataToSave);
                        $imageToSave->save();
                        DB::commit();
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    /**
     * Create a thumbnail for existing Day image.
     *
     * @param Day    $day      Day to associate thumbnail with.
     * @param string $fileName Image file to have thumbnail made from.
     * @param int    $width    Thumbnail width.
     * @param int    $height   Thumbnail height.
     *
     * @return \Intervention\Image\Image|null
     */
    public static function createThumbnail(Day $day, string $fileName, int $width, int $height)
    {
        $width = !empty($width) ? $width : Constants::DEFAULT_THUMBNAIL_WIDTH;
        $height = !empty($height) ? $height : Constants::DEFAULT_THUMBNAIL_HEIGHT;

        self::createThumbnailDirectory($day);
        $thumbNailDir = self::createThumbnailDirectoryPath($day);

        $img = InterventionImageFacade::make($fileName)->resize(
            $width, $height, function ($constraint) {
                $constraint->aspectRatio();
            }
        );

        $thumbFile = self::createThumbnailFileName($img, $img->width(), $img->height());

        $fullThumbPath = storage_path('app') .
            Constants::DIR_SEPARATOR .
            $thumbNailDir .
            Constants::DIR_SEPARATOR .
            $thumbFile;

        if(!file_exists($fullThumbPath)) {
            $img->save($fullThumbPath, 100);

            return $img;
        }

        return InterventionImageFacade::make($fullThumbPath);

    }

    /**
     * Generate path where thumbnail can be viewed publicly.
     *
     * @param Day               $day        Day which thumbnail is associated.
     * @param InterventionImage $thumbImage The thumbnail image (Intervention\Image\Image).
     *
     * @return string|null The thumbnail path.
     */
    public static function createThumbnailPublicFilePath(Day $day, InterventionImage $thumbImage)
    {
        if (!empty($day) && !empty($thumbImage)) {
            return DiskUtility::getPublicImagesPath() .
                "day" . $day->number . Constants::DIR_SEPARATOR .
                Constants::THUMBNAIL_DIR_NAME . Constants::DIR_SEPARATOR .
                $thumbImage->basename;
        }
        return null;
    }

    /**
     * Create thumbnail name from Intervention\Image\Image instance.
     *
     * @param \Intervention\Image\Image $image       Image to have
     *                                               thumbnail
     *                                               name
     *                                               generated
     *                                               for.
     * @param int                       $thumbWidth  Thumbnail width.
     * @param int                       $thumbHeight Thumbnail height.
     *
     * @return string|null The thumbnail file name, including extension.
     */
    public static function createThumbnailFileName(InterventionImage $image, int $thumbWidth, int $thumbHeight): string
    {
        if (!empty($image)) {

            $thumbWidth = !empty($thumbWidth) ? $thumbWidth : Constants::DEFAULT_THUMBNAIL_WIDTH;
            $thumbHeight = !empty($thumbHeight) ? $thumbHeight : Constants::DEFAULT_THUMBNAIL_HEIGHT;

            return $image->filename . '_' .
                $thumbWidth . 'x' . $thumbHeight .
                Constants::DOT_SEPARATOR . $image->extension;
        }
        return null;
    }

    /**
     * Create thumbnail directory name for a given day.
     *
     * @param Day $day Day instance for thumbnail directory name.
     *
     * @return string The generated thumbnail directory path.
     */
    public static function createThumbnailDirectoryPath(Day $day): string
    {
        if (!empty($day)) {
            return DiskUtility::getImagesStoragePath() . "day" .
                $day->number . Constants::DIR_SEPARATOR . Constants::THUMBNAIL_DIR_NAME;
        }
        return null;
    }

    /**
     * Create directory where thumbnail file is to be stored.
     *
     * @param Day $day Day to associate thumbnail directory with.
     *
     * @return bool True if successfully created, false otherwise.
     */
    public static function createThumbnailDirectory(Day $day)
    {
        if (!empty($day)) {
            $thumbNailDir = self::createThumbnailDirectoryPath($day);

            $thumbNailDirCheck = DiskUtility::getDisk()
                ->exists(
                    "images/day" . $day->number .
                    Constants::DIR_SEPARATOR .
                    Constants::THUMBNAIL_DIR_NAME
                );

            if (!$thumbNailDirCheck) {
                Storage::makeDirectory($thumbNailDir);
                return true;
            } else if ($thumbNailDirCheck) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Remove image thumbnail directory for given day.
     *
     * @param Day $day Day for which thumbnail directory to remove.
     *
     * @return void
     */
    public static function removeThumbNailDirectory(Day $day): void
    {
        if (!empty($day)) {

            $dir = "images/day" . $day->number . "/" . Constants::THUMBNAIL_DIR_NAME;

            if (DiskUtility::getDisk()->exists($dir)) {

                $thumbNails = Storage::allFiles(
                    DiskUtility::getImagesStoragePath() .
                    "day" . $day->number . "/" .
                    Constants::THUMBNAIL_DIR_NAME
                );

                foreach ($thumbNails as $t) {
                    unlink(storage_path("app") . Constants::DIR_SEPARATOR . $t);
                }

                $fullPath = storage_path("app") . Constants::DIR_SEPARATOR .
                    DiskUtility::getImagesStoragePath() .
                    "day" . $day['number'] . Constants::DIR_SEPARATOR .
                    Constants::THUMBNAIL_DIR_NAME;

                if(file_exists($fullPath)) {
                    rmdir($fullPath);
                }
            }
        }
    }

    public static function renderImageThumbHtml(Image $image)
    {
        return self::renderImage($image, 1, true);
    }

    public static function renderImage(Image $image, int $shrinkBy = 1, bool $isThumb = false)
    {
        $altTitle = "Day " . $image->day()->first()->number .
            " (" . $image->date_time . ")";

        !empty($image->image_description) ?
            $altTitle . ": " . $image->image_description: null;

        if($isThumb) {

            $imageLink = route("images.show", $image->id);

            return '<a href="'. $imageLink . '"><img src="' .
                asset($image->thumbnail_file_path) . '" width="' .
                $image->thumbnail_file_width . '" height="' .
                $image->thumbnail_file_height . '" alt="' .
                $altTitle .'" title="' . $altTitle . '" style="display: block;"></a>';
        }

        $fullPathToImageFile = url($image->file_path);
        $img = InterventionImageFacade::make($image->file_path);
        $width = $img->width() / $shrinkBy;
        $img->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return '<a href="'. $fullPathToImageFile . '" target="_blank"><img src="' .
            $fullPathToImageFile . '" width="' .
            $img->getWidth() . '" height="' .
            $img->getHeight() . '" alt="' .
            $altTitle .'" title="' . $altTitle . '"></a>';
    }

    public static function renderImageLatitude(Image $image)
    {
        return !empty($image->gps_latitude) && !empty($image->gps_latitude_ref) ?
            $image->gps_latitude . ' (' . $image->gps_latitude_ref . ')' : null;
    }

    public static function renderImageLongitude(Image $image)
    {
        $display = !empty($image->gps_longitude) && !empty($image->gps_longitude_ref) ?
            $image->gps_longitude . ' (' . $image->gps_longitude_ref . ')' : null;

        return $display;
    }

    public static function renderImageDayLinkHtml(Image $image)
    {
        $dayNumber = $image->day()->first()->number;
        $title = "Day " . $dayNumber;
        $url = route("days.show", $dayNumber);
        return '<a href="' . $url . '" title="' . $title . '">' . $title . '</a>';
    }

    public static function formatImageDate(Image $image, bool $withTzInfo = false)
    {
        if(!empty($image->date_time))
        {
            $date = Carbon::parse($image->date_time);
            $date = $date->format('jS \o\f F Y, \a\t g:i:s a');
            return $date;
        }
        return "No date available";
    }

    public static function imageTzInfo(Image $image)
    {
        $tzInfo = [];

        $timezone = $image->timezone()->first();
        if(!empty($timezone)){
            $tzInfo['abbreviation'] = $timezone->abbreviation;
            $tzInfo['timezone_name'] = $timezone->timezone_name;
            $tzInfo['timezone_id'] = $timezone->timezone_id;
            return $tzInfo;
        }
        return [];
    }
}
