<?php


namespace App\Utilities;


use Carbon\Carbon;
use DateTime;
use Spatie\GoogleTimeZone\Exceptions\GoogleTimeZoneException;
use Spatie\GoogleTimeZone\Facades\GoogleTimeZone;

class GpsUtility
{
    private $latitude;
    private $longitude;
    private $dateTime;

    /**
     * @param  string $latitudeRef
     * @param  array  $latitude
     * @return $this
     */
    public function setLatitude(string $latitudeRef, array $latitude): self
    {
        $this->latitude = self::calculateLatitude($latitudeRef, $latitude);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLatitude()
    {
        return !empty($this->latitude) ? $this->latitude : null;
    }

    /**
     * @param  string $longitudeRef
     * @param  array  $longitude
     * @return $this
     */
    public function setLongitude(string $longitudeRef, array $longitude): self
    {
        $this->longitude = self::calculateLongitude($longitudeRef, $longitude);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLongitude()
    {
        return !empty($this->longitude) ? $this->longitude : null;
    }

    /**
     * @param  Carbon $dateTime
     * @return $this
     */
    public function setDateTime(Carbon $dateTime)
    {
        $this->dateTime = $dateTime;
        return $this;
    }

    /**
     * Get timezone information with a date and coordinates,
     * using Google Timezone API.
     *
     * @return array|null
     */
    public function getTimezone()
    {
        try {
            $date = new DateTime($this->dateTime->format('Y-m-d H:i:s'));
            return GoogleTimeZone::setTimestamp($date)
                ->getTimeZoneForCoordinates($this->getLatitude(), $this->getLongitude());
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Convert latitude value to positive or negative,
     * depending on northern (N) or southern (S) hemisphere
     * reference.
     *
     * @param  string $latitudeRef Northern (N) or southern (S) hemisphere reference.
     * @param  array  $latitude    Latitude value.
     * @return float|null
     */
    public static function calculateLatitude(string $latitudeRef, array $latitude)
    {
        if(!empty($latitude) && is_array($latitude)) {
            if(!empty($latitude[0]) && !empty($latitude[1]) && !empty($latitude[2])) {

                $math = new EvalMath();
                $seconds = $math->e($latitude[0]);
                $minutes = $math->e($latitude[1]);
                $hours = $math->e($latitude[2]);
                $latitude = $seconds
                    + ($minutes / 60)
                    + ($hours / 3600);

                if ($latitude > 90 || $latitude < -90) {
                    return null;
                }

                // Latitude should be positive when Ref is 'N'.
                // Latitude should be negative when Ref is 'S'.
                if (strtoupper($latitudeRef) == 'N' && $latitude < 0.0
                    || strtoupper($latitudeRef) == 'S' && $latitude > 0.0
                ) {
                    return $latitude * -1.0;
                } else if (strtoupper($latitudeRef) == 'N' && $latitude > 0.0
                    || strtoupper($latitudeRef) == 'S' && $latitude < 0.0
                ) {
                    return $latitude;
                }  else {
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * Convert longitude value to positive or negative,
     * depending on eastern (E) or western (W) hemisphere
     * reference.
     *
     * @param  string $longitudeRef Eastern (E) or Western (W) hemisphere reference.
     * @param  array  $longitude    Longitude value.
     * @return float|null
     */
    public static function calculateLongitude(string $longitudeRef, array $longitude)
    {
        if(!empty($longitude) && is_array($longitude)) {
            if(!empty($longitude[0]) && !empty($longitude[1]) && !empty($longitude[2])) {

                $math = new EvalMath();
                $seconds = $math->e($longitude[0]);
                $minutes = $math->e($longitude[1]);
                $hours = $math->e($longitude[2]);
                $longitude = $seconds
                    + ($minutes / 60)
                    + ($hours / 3600);

                if ($longitude > 180 || $longitude < -180) {
                    return null;
                }

                // Longitude should be positive when Ref is 'E'.
                // Longitude should be negative when Ref is 'W'.
                if (strtoupper($longitudeRef) == 'E' && $longitude < 0.0
                    || strtoupper($longitudeRef) == 'W' && $longitude > 0.0
                ) {
                    return $longitude * -1.0;
                } else if (strtoupper($longitudeRef) == 'E' && $longitude > 0.0
                    || strtoupper($longitudeRef) == 'W' && $longitude < 0.0
                ) {
                    return $longitude;
                } else {
                    return null;
                }
            }
        }
        return null;
    }
}
