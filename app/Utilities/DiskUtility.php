<?php

namespace App\Utilities;

use App\Constants;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class DiskUtility
{
    /**
     * @return Filesystem|FilesystemAdapter
     */
    public static function getDisk()
    {
        $default = Storage::disk('public');

        $appEnvEnvironment = Config::get('app.env');

        if(!empty($appEnvEnvironment)) {
            switch ($appEnvEnvironment){
            case "local":
            case "production":
                return Storage::disk('public');
            case "testing":
            case "test":
                return Storage::disk('testing');
            default:
                return $default;
            }
        }
        return $default;
    }

    /**
     * @return string
     */
    public static function getImagesStoragePath()
    {
        $default =  Constants::PUBLIC_IMAGES_STORAGE_PATH;

        $appEnvEnvironment = Config::get('app.env');

        if(!empty($appEnvEnvironment)) {
            switch ($appEnvEnvironment){
            case "local":
            case "production":
                return Constants::PUBLIC_IMAGES_STORAGE_PATH;
            case "testing":
            case "test":
                return Constants::TESTING_IMAGES_STORAGE_PATH;
            default:
                return $default;
            }
        }
        return $default;
    }

    public static function getPublicImagesPath()
    {
        $default = Constants::IMAGES_STORAGE_PATH;

        $appEnvEnvironment = Config::get('app.env');

        if(!empty($appEnvEnvironment)) {
            switch ($appEnvEnvironment){
            case "local":
            case "production":
                return Constants::IMAGES_STORAGE_PATH;
            case "testing":
            case "test":
                return Constants::IMAGES_TESTING_PATH;
            default:
                return $default;
            }
        }
        return $default;
    }

    public static function formatBytes($bytes, $precision = 2) {
        $units = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        $i = 0;

        while($bytes > 1024) {
            $bytes /= 1024;
            $i++;
        }
        return round($bytes, $precision) . ' ' . $units[$i];
    }
}
