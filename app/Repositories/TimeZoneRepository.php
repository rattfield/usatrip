<?php

namespace App\Repositories;

use App\Models\TimeZone;
use App\Repositories\BaseRepository;

/**
 * Class TimeZoneRepository
 *
 * @package App\Repositories
 * @version November 11, 2019, 2:45 am UTC
 */

class TimeZoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dst_offset',
        'raw_offset',
        'timezone_id',
        'timezone_name',
        'abbreviation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TimeZone::class;
    }
}
