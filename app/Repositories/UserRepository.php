<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class UserRepository
 *
 * @package App\Repositories
 * @version October 11, 2019, 8:41 am UTC
 */

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'is_admin'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create($input)
    {
        $input['is_admin'] = $input['is_admin'] ?? 0;
        $input = User::cleanMultipleInput($input);

        $model = $this->model->newInstance($input);

        $model->save();

        return $model;
    }

    /**
     * Update model record for given id
     *
     * @param array $input
     * @param int   $id
     *
     * @return Builder|Builder[]|Collection|Model
     * @throws MassAssignmentException
     * @throws ModelNotFoundException
     */
    public function update($input, $id)
    {
        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);
        $input['is_admin'] = intval($model->is_admin);
        $input = User::cleanMultipleInput($input);

        $model->fill($input);

        $model->save();

        return $model;
    }

    /**
     * Configure the Model
     */
    public function model()
    {
        return User::class;
    }
}
