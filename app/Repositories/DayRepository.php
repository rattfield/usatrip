<?php

namespace App\Repositories;

use App\Models\Day;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class DayRepository
 *
 * @package App\Repositories
 * @version October 9, 2019, 9:57 am UTC
 */

class DayRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'number',
        'title',
        'date',
        'summary',
        'keywords',
        'description'
    ];

    /**
     * Find model record for given id
     *
     * @param int   $number
     * @param array $columns
     *
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function find($number, $columns = ['*'])
    {
        $query = $this->model->newQuery();

        return $query->where('number', '=', $number)->first();
    }

    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create($input)
    {
        $input = Day::cleanMultipleInput($input);

        $model = $this->model->newInstance($input);

        $model->save();

        return $model;
    }

    /**
     * Update model record for given id
     *
     * @param array $input
     * @param int   $number
     *
     * @return Builder|Builder[]|Collection|Model
     * @throws MassAssignmentException
     */
    public function update($input, $number)
    {
        $query = $this->model->newQuery();

        $model = $query->where('number', '=', $number)->first();

        $input = Day::cleanMultipleInput($input);

        $model->fill($input);

        $model->save();

        return $model;
    }

    /**
     * @param int $number
     *
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function delete($number)
    {
        $query = $this->model->newQuery();

        $model = $query->where('number', '=', $number)->first();

        return $model->delete();
    }

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Day::class;
    }
}
