<?php

namespace App\Repositories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ImageRepository
 *
 * @package App\Repositories
 * @version October 30, 2019, 9:49 am UTC
 */

class ImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'file_name',
        'file_path',
        'file_date_time',
        'file_size',
        'file_width',
        'file_height',
        'mime_type',
        'image_description',
        'orientation',
        'date_time',
        'gps_latitude_ref',
        'gps_latitude',
        'gps_longitude_ref',
        'gps_longitude',
        'gps_altitude_ref',
        'gps_altitude'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create($input)
    {
        $input = Image::cleanMultipleInput($input);

        $model = $this->model->newInstance($input);

        $model->save();

        return $model;
    }

    /**
     * Update model record for given id
     *
     * @param array $input
     * @param int   $id
     *
     * @return Builder|Builder[]|Collection|Model
     * @throws MassAssignmentException
     * @throws ModelNotFoundException
     */
    public function update($input, $id)
    {
        $input = Image::cleanMultipleInput($input);

        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        $model->fill($input);

        $model->save();

        return $model;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Image::class;
    }
}
