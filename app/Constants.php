<?php


namespace App;


class Constants
{
    const DIR_SEPARATOR = '/';
    const ALLOWED_MIME_TYPES = ['image/jpeg'];
    const MIME_TYPE_JPEG = "image/jpeg";
    const IMAGES_STORAGE_PATH = "storage" . self::DIR_SEPARATOR . "images" . self::DIR_SEPARATOR;
    const IMAGES_TESTING_PATH = "testing" . self::DIR_SEPARATOR . "images" . self::DIR_SEPARATOR;
    const PUBLIC_IMAGES_STORAGE_PATH = "public" . self::DIR_SEPARATOR . "images" . self::DIR_SEPARATOR;
    const TESTING_IMAGES_STORAGE_PATH = "testing" . self::DIR_SEPARATOR . "images" . self::DIR_SEPARATOR;
    const CMD_PROGRESS_BAR_FORMAT = "%current% of %max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s%";
    const DEFAULT_THUMBNAIL_WIDTH = 150;
    const DEFAULT_THUMBNAIL_HEIGHT = 84;
    const THUMBNAIL_DIR_NAME = "thumbnails";
    const DOT_SEPARATOR = '.';
}
