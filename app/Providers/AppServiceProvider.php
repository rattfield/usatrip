<?php

namespace App\Providers;

use App\Console\Commands\MakeModelCommand;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->extend(
            'command.model.make', function ($command, $app) {
                return new MakeModelCommand($app['files']);
            }
        );
    }
}
