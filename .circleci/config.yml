version: 2
jobs:
  build:
    docker:
      - image: circleci/php:7.3-fpm
        environment:
          MYSQL_ROOT_PASSWORD: password
          MYSQL_HOST: 127.0.0.1
    parallelism: 1
    steps:
      - checkout

      - run:
          name: Install things we need
          command: |
            sudo apt update && sudo apt -f -y upgrade && sudo apt -f -y dist-upgrade
            sudo apt install -y libzip-dev
            sudo apt install -y zlib1g-dev
            sudo apt install -y libicu-dev
            sudo apt install -y g++
            sudo apt install -y libxml2-dev
            sudo apt install -y curl
            sudo apt install -y nodejs
            sudo apt install -y npm
            sudo apt install -y libfreetype6-dev
            sudo apt install -y libpng-dev
            sudo apt install -y libjpeg-dev
            sudo apt install -y libwebp-dev
            sudo apt install -y imagemagick
            sudo apt install -y libmagickcore-dev
            sudo apt install -y libmagickwand-dev
            sudo apt install -y node-gyp
            sudo apt install -y libnode-dev
            sudo apt install -y redis-server
            sudo apt install -y redis-tools
            sudo apt install -y apache2
            sudo apt install -y lsb-release
            sudo apt install -y default-mysql-client
            sudo apt install -y default-mysql-server
            sudo apt install -y gcc
            sudo apt install -y make
            sudo apt install -y autoconf
            sudo apt install -y libc-dev
            sudo apt install -y pkg-config
            sudo apt install -f libgeoip-dev
            sudo echo 'memory_limit=-1' | sudo tee -a /usr/local/etc/php/php.ini

      - run:
          name: Install PHP exts
          command: |
            sudo pecl channel-update pecl.php.net && sudo pecl install -f imagick redis geoip-beta
            sudo bash -c "echo extension=geoip.so > /usr/local/etc/php/conf.d/geoip.ini"
            sudo docker-php-ext-install zip
            sudo docker-php-ext-configure intl
            sudo docker-php-ext-install intl
            sudo docker-php-ext-install pdo_mysql
            sudo docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd
            sudo docker-php-ext-install bcmath
            sudo docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
            sudo docker-php-ext-install -j$(nproc) gd
            sudo docker-php-ext-install exif
            sudo docker-php-ext-configure exif --enable-exif

      - run:
          name: Enable PHP exts
          command: |
            sudo docker-php-ext-enable zip
            sudo docker-php-ext-enable bcmath
            sudo docker-php-ext-enable pdo_mysql
            sudo docker-php-ext-enable gd
            sudo docker-php-ext-enable imagick
            sudo docker-php-ext-enable redis
            sudo docker-php-ext-enable geoip

      - run:
          name: Check PHP exts are enabled
          command: |
            php -m | grep zip >> /dev/null && echo "ext-zip enabled" || echo "ext-zip not enabled"
            php -m | grep bcmath >> /dev/null && echo "ext-bcmath enabled" || echo "ext-bcmath not enabled"
            php -m | grep pdo_mysql >> /dev/null && echo "ext-pdo_mysql enabled" || echo "ext-pdo_mysql not enabled"
            php -m | grep gd >> /dev/null && echo "ext-gd enabled" || echo "ext-gd_mysql not enabled"
            php -m | grep imagick >> /dev/null && echo "ext-imagick enabled" || echo "ext-imagick not enabled"
            php -m | grep redis >> /dev/null && echo "ext-redis enabled" || echo "ext-redis not enabled"
            php -m | grep geoip >> /dev/null && echo "ext-geoip enabled" || echo "ext-geoip not enabled"

      - run:
          name: Enable Apache exts and configs
          command: |
            sudo a2enmod proxy_fcgi setenvif && sudo a2enconf && sudo service apache2 restart

      - run:
          name: Fix MySQL socket config
          command: |
            sudo sh -c "echo 'pdo_mysql.default_socket=/var/run/mysqld/mysqld.sock' > /usr/local/etc/php/conf.d/pdo.ini"

      - run:
          name: Start MySQL database server
          command: |
            sudo service mysql start
            sleep 2
            ps aux | grep mysql

      - run:
          name: Create MySQL non-admin user
          command: |
            sudo mysql -u root -ppassword -e "CREATE user 'usatrip' IDENTIFIED BY 'password'"

      - run:
          name: Test MySQL connection for created user 'usatrip'
          command: |
            php test_mysql_connection.php

      - run:
          name: Create MySQL database for project
          command: |
            sudo mysql -u root -ppassword -e "CREATE DATABASE usatrip;"

      - run:
          name: Grant all privileges for user 'usatrip' to database 'usatrip'
          command: |
            sudo mysql -u root -ppassword -e "GRANT ALL PRIVILEGES ON usatrip.* to 'usatrip'@'localhost' IDENTIFIED BY 'password'; FLUSH PRIVILEGES;"

      - run:
          name: Install Yarn with NodeJS / NPM.
          command: |
            sudo npm install -g -y yarn && sudo chmod u+x /usr/local/bin/yarn
            export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

      # Download and cache Yarn dependencies
      - restore_cache:
          name: Restore Yarn cache from package.json
          keys:
            - node-v1-{{ checksum "package.json" }}
            - node-v1-

      - save_cache:
          name: Save Yarn cache
          key: node-v1-{{ checksum "package.json" }}
          paths:
            - node_modules

      - restore_cache:
          name: Restore Yarn Package Cache
          keys:
            - yarn-packages-{{ checksum "yarn.lock" }}

      - run:
          name: Install Yarn Dependencies
          command: sudo yarn install --frozen-lockfile --force

      - save_cache:
          name: Save Yarn Package Cache
          key: yarn-packages-{{ checksum "yarn.lock" }}
          paths:
            - ~/.cache/yarn

      # Download and cache Composer dependencies
      - restore_cache:
          name: Download and cache Composer dependencies
          keys:
            # "composer.lock" can be used if it is committed to the repo
            - v1-dependencies-{{ checksum "composer.json" }}
            # fallback to using the latest cache if no exact match is found
            - v1-dependencies-

      - run:
          name: Install Composer packages.
          command: |
            cp .env.testing .env
            chmod -R 755 bootstrap && mkdir -p bootstrap/cache
            chmod -R ugo+rwx bootstrap/cache
            mkdir -p storage/framework/{session,views,cache}
            chmod -R 777 storage/framework
            sudo chown -R www-data:www-data storage/framework            
            composer install -n --prefer-dist

      - run:
          name: Run project database migrations.
          command: |
            php artisan migrate --seed --env=testing --force
            php artisan images:process all

      - run:
          name: Run PHPUnit tests
          command: |
            chmod u+x run_phpunit
            ./run_phpunit

workflows:
  version: 2
  build-and-deploy:
    jobs:
      - build
      - deploy:
          requires:
            - build
          filters:
            branches:
              only: sequential-branch-filter
